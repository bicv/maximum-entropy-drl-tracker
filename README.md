# Maximum-Entropy DRL Tracker
This repository contains code for training and testing a maximum-entropy deep reinforcement learning (DRL) tracker in 
neuron images. It uses a variant of the soft actor-critic [^fn1] with options to fix the temperature parameter or enable 
automatic entropy tuning. The tracker builds on the prior work done by Dai *et al* [^fn2] and is introduced as an 
improvement to the training process. 

## Prerequisites 
Modules required and versions used during the project: 

* python 3.7.3
* pytorch 1.0.1
* numpy 1.16.3
* scipy 1.3.0
* opencv 4.1.0
* matplotlib 3.1.0
* tqdm 4.32.1


## Instructions to run the code
### Train
    python train_sac.py 

## Reference
To cite this code in publications:  
Shafa Balaram, Kai Arulkumaran, Tianhong Dai, Anil A Bharath, "A Maximum Entropy Deep Reinforcement Learning Neural Tracker", accepted for 10th International Workshop on Machine Learning in Medical Imaging (MLMI 2019). October 13, 2019, Shenzhen, China.
 
 
[^fn1]: Tuomas Haarnoja, Aurick-Zhou, Kristian Hartikainen, George Tucker, Sehoon Ha, Jie Tan, Vikash Kumar, Henry Zhu, 
Abhishek Gupta, Pieter Abbeel, et al. Soft actor-critic algorithms and applications. arXiv preprint arXiv:1812.05905, 2018.
[^fn2]:  Tianhong Dai, Magda Dubois, Kai Arulkumaran, Jonathan Campbell, Cher Bass,
Benjamin Billot, Fatmatulzehra Uslu, Vincenzo de Paola, Claudia Clopath, and
Anil Anthony Bharath. Deep reinforcement learning for subpixel neural tracking. In Proceedings of the International Conference on Medical Imaging with Deep
Learning, pages 130–150, 2019.