""" This code define the main body of the environment for the DRL axon tracker.
    Author: Tianhong Dai
    Modified by Shafa Balaram for the maximum-entropy DRL neural tracker. """

import numpy as np
from generative_model.generator import axon_generator
import cv2
import scipy
import scipy.interpolate
import copy 
import torch
from .agent import agent_for_tracking
from .utils import add_border
from .utils import generate_gaussian_kde


# this will create the environment for the RL based axon tracking...
class axon_env:
    def __init__(self, args):
        self.args = args
        # define the agent
        self.agent = agent_for_tracking([0, 0], self.args.precision, self.args.render)
        # define the generator...
        self.generator = axon_generator(self.args)
    
    # reset the environment
    def reset(self, seed_num):
        self.args.seed_num = seed_num
        # init the parameters...
        self.stop = False
        self.success = False
        self.counter = 0
        self.length_counter = 0
        # start to generate the images...
        self.entire_img, self.reward_map, gt_points = self.generator.generate_imgs(seed_num)
        # add the border to the axon images...
        self.entire_img = add_border(self.entire_img, self.args.border_size)
        # add the border to the reward map...
        self.reward_map = add_border(self.reward_map, self.args.border_size)
        # copy one for the visualize...
        self.visual_img = copy.deepcopy(self.entire_img)
        # check if reverse the color...
        if self.args.reverse_color:
            self.entire_img = 1 - self.entire_img
        # interpolate the entire image for the subpixel accuracy
        self.interp_img, self.interp_reward = self._interpolate_imgs(self.entire_img, self.reward_map)
        # get the previous path image...
        self.historical_path = np.zeros(self.interp_img.shape)
        if self.args.reverse_color:
            self.historical_path = 1 - self.historical_path
        # process the gt images...
        self.interp_gt = copy.deepcopy(self.interp_reward)
        if self.args.reverse_color:
            self.interp_gt = 1 - self.interp_gt
        # get the start point and end point of the axon
        self.start_point = np.round(gt_points[0, :] + self.args.border_size) * (10 ** self.args.precision)
        self.end_point = np.round(gt_points[-1, :] + self.args.border_size) * (10 ** self.args.precision)
        # start to reset the position of the agent...
        self.agent.reset(self.start_point)
        # get the initial direction vector
        self.pre_dir_vec = gt_points[1, :] - gt_points[0, :]
        # start to extract the states from the images...
        small_state, large_state, history_state, gt_state = self._extract_states()
        # 4 observations
        small_state = np.stack([small_state, small_state, small_state, small_state], axis=0)
        large_state = np.stack([large_state, large_state, large_state, large_state], axis=0)
        history_state = np.stack([history_state, history_state, history_state, history_state], axis=0)
        gt_state = np.stack([gt_state, gt_state, gt_state, gt_state], axis=0)
        # concatenate the observations together
        self.state = np.concatenate((small_state, large_state, history_state), axis=0)
        self.extra_state = np.concatenate((small_state, large_state, history_state, gt_state), axis=0)
        self.state_tensor = torch.tensor(self.state, dtype=torch.float32).unsqueeze(dim=0)
        self.extra_state_tensor = torch.tensor(self.extra_state, dtype=torch.float32).unsqueeze(dim=0)
        return self.state_tensor, self.extra_state_tensor

    # take the action
    def step(self, action):
        self.length_counter += 1
        # get the previous location
        prev_loc = copy.deepcopy(self.agent.pos)
        # take the actions...
        action_interp = np.round(action, self.args.precision) * (10 ** self.args.precision)
        action_interp = action_interp.astype(np.int)
        self.agent.take_actions(action_interp)
        # get the reward from the env...
        reward, path_recorder = self._get_reward(prev_loc)
        # will add the path to the historical path
        self._add_historical_path(path_recorder)
        # check the terminal
        terminal = self._check_terminal()
        # if length is exceed, set terminal as True
        if self.length_counter >= self.args.episode_length:
            terminal = True
        # extract the states
        next_small_state, next_large_state, next_history_state, next_gt_state = self._extract_states()
        # concatenate small views
        small_state = self.state[0:3, :, :]
        next_small_state = next_small_state[np.newaxis, :, :]
        small_state = np.concatenate((next_small_state, small_state), axis=0)
        # concatenate downscaled large views
        large_state = self.state[4:7, :, :]
        next_large_state = next_large_state[np.newaxis, :, :]
        large_state = np.concatenate((next_large_state, large_state), axis=0)  
        # concatenate historical paths
        history_state = self.state[8:11, :, :]
        next_history_state = next_history_state[np.newaxis, :, :]
        history_state = np.concatenate((next_history_state, history_state), axis=0)  
        # concatenate views from binary ground truth map
        gt_state = self.extra_state[12:15, :, :]
        next_gt_state = next_gt_state[np.newaxis, :, :]
        gt_state = np.concatenate((next_gt_state, gt_state), axis=0)
        # concatenate all 12 or 16 views for the actor-critic networks
        self.state = np.concatenate((small_state, large_state, history_state), axis=0)
        self.extra_state = np.concatenate((small_state, large_state, history_state, gt_state), axis=0)
        # tensorise for actor-critic networks
        self.state_tensor = torch.tensor(self.state, dtype=torch.float32).unsqueeze(dim=0)
        self.extra_state_tensor = torch.tensor(self.extra_state, dtype=torch.float32).unsqueeze(dim=0)
        return self.state_tensor, self.extra_state_tensor, reward, terminal

    # get the reward from the environment...  
    def _get_reward(self, prev_loc):
        # record the path...
        path_recorder = []
        # set the reward
        reward = 0
        # cal the direction vector of the current location
        dir_vec, stop = self._cal_dir_vec(prev_loc)
        # calculate the dot product
        dot_product = np.dot(self.pre_dir_vec, dir_vec)
        if stop:
            path_recorder.append(prev_loc)
            path_recorder = np.array(path_recorder)
            # not recommend stay
            reward = -1
        else:            
            step_size = dir_vec / self.args.sample_point
            current_point = copy.deepcopy(prev_loc)
            path_recorder.append(current_point)
            for _ in range(self.args.sample_point):
                current_point = current_point + step_size 
                # append to recorder
                path_recorder.append(current_point)
            # make them into np array...
            path_recorder = np.array(path_recorder)
            path_recorder = np.floor(path_recorder)
            path_recorder = path_recorder.astype(np.int)
            # remove the repeated term
            path_recorder = np.unique(path_recorder, axis=0)
            # start to accumulate the reward...
            for idx in range(path_recorder.shape[0]):
                reward = reward + self.interp_reward[path_recorder[idx, 0], path_recorder[idx, 1]]
            # if the dot product is negative, punish it
            if dot_product <= 0:
                reward = -1 * reward
            # re-assign the vector...
            self.pre_dir_vec = dir_vec
        return reward, path_recorder 
    
    # check whether terminal state is reached
    def _check_terminal(self):
        lt_size_interp = self.args.lt_size * (10 ** self.args.precision) - 1
        diff_large = int((lt_size_interp - 1) / 2)
        # calculate the distance to the end point
        dist = np.sqrt(np.sum((self.agent.pos - self.end_point) ** 2))
        margin = self.args.border_size 
        if (self.agent.pos[1] >= (margin-1) * (10 ** self.args.precision) and \
            self.agent.pos[1] <= (margin + 1 + self.args.width) * (10 ** self.args.precision) and \
            self.agent.pos[0] >= (margin-1) * (10 ** self.args.precision) and \
            self.agent.pos[0] <= (margin + 1 + self.args.height) * (10 ** self.args.precision) and \
            dist > 5 * (10 ** self.args.precision)) :
            terminal = False         
        else:
            terminal = True 
            if (self.agent.pos[1] >= (margin-1) * (10 ** self.args.precision) and \
                self.agent.pos[1] <= (margin + 1 + self.args.width) * (10 ** self.args.precision) and \
                self.agent.pos[0] >= (margin-1) * (10 ** self.args.precision) and \
                self.agent.pos[0] <= (margin + 1 + self.args.height) * (10 ** self.args.precision) and \
                dist <= 5 * (10 ** self.args.precision)) :
                self.success = True
        return terminal

    # add the historical path...
    def _add_historical_path(self, path):
        for idx in range(path.shape[0]):
            if self.args.reverse_color:
                self.historical_path[path[idx,0]-int((10**self.args.precision)/2):path[idx,0]+int((10**self.args.precision)/2)+1, \
                        path[idx,1]-int((10**self.args.precision)/2):path[idx,1]+int((10**self.args.precision)/2)+1] = 0
            else:
                self.historical_path[path[idx,0]-int((10**self.args.precision)/2):path[idx,0]+int((10**self.args.precision)/2)+1, \
                        path[idx,1]-int((10**self.args.precision)/2):path[idx,1]+int((10**self.args.precision)/2)+1] = 1

    
    # calculate the direction vector
    def _cal_dir_vec(self, prev_loc):
        dir_vec = self.agent.pos - prev_loc
        if dir_vec[0] == 0 and dir_vec[1] == 0:
            stop = True
        else:
            stop = False
        return dir_vec, stop 

    # interpolate entire images...
    def _interpolate_imgs(self, entire_img, gt_img):
        y_axis = np.arange(0, self.args.height + 2 * self.args.border_size)
        x_axis = np.arange(0, self.args.width + 2 * self.args.border_size)
        interpolate_img = scipy.interpolate.interp2d(x_axis, y_axis, entire_img)
        interpolate_gt = scipy.interpolate.interp2d(x_axis, y_axis, gt_img)
        x_axis_interp = np.arange(0, self.args.width + 2 * self.args.border_size, 1 / (10 ** self.args.precision))
        y_axis_interp = np.arange(0, self.args.height + 2 * self.args.border_size, 1 / (10 ** self.args.precision))
        interp_img = interpolate_img(x_axis_interp, y_axis_interp)
        interp_gt = interpolate_gt(x_axis_interp, y_axis_interp)
        return interp_img, interp_gt

    # extract the states from the environment...
    def _extract_states(self):
        interp_st_size = self.args.st_size * (10 ** self.args.precision) - 1
        interp_lt_size = self.args.lt_size * (10 ** self.args.precision) - 1
        # get the state of the small size tracker... 
        small_state = self.interp_img[self.agent.pos[0]-int((interp_st_size-1)/2):self.agent.pos[0]+int((interp_st_size-1)/2)+1, \
                                    self.agent.pos[1]-int((interp_st_size-1)/2):self.agent.pos[1]+int((interp_st_size-1)/2)+1]
        small_state = cv2.resize(small_state, (self.args.state_size, self.args.state_size))
        # get the state of the large size tracker...
        large_state = self.interp_img[self.agent.pos[0]-int((interp_lt_size-1)/2):self.agent.pos[0]+int((interp_lt_size-1)/2)+1, \
                                    self.agent.pos[1]-int((interp_lt_size-1)/2):self.agent.pos[1]+int((interp_lt_size-1)/2)+1]
        large_state = cv2.resize(large_state, (self.args.state_size, self.args.state_size))
        # get the state of the historical path
        history_state = self.historical_path[self.agent.pos[0]-int((interp_st_size-1)/2):self.agent.pos[0]+int((interp_st_size-1)/2)+1, \
                                    self.agent.pos[1]-int((interp_st_size-1)/2):self.agent.pos[1]+int((interp_st_size-1)/2)+1]
        history_state = cv2.resize(history_state, (self.args.state_size, self.args.state_size))
        # get the ground truth state...
        gt_state = self.interp_gt[self.agent.pos[0]-int((interp_st_size-1)/2):self.agent.pos[0]+int((interp_st_size-1)/2)+1, \
                                    self.agent.pos[1]-int((interp_st_size-1)/2):self.agent.pos[1]+int((interp_st_size-1)/2)+1]
        gt_state = cv2.resize(gt_state, (self.args.state_size, self.args.state_size))
        return small_state, large_state, history_state, gt_state
