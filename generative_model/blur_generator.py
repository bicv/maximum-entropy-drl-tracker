# Author: Tianhong Dai

import numpy as np
from generative_model.utils import matlab_style_gauss2D 
from scipy.ndimage.filters import convolve

"""
This module is used to generate the blur in the image

"""

def get_blur(img):
    # create the x slice and y_slice...
    x_slice = np.arange(0, img.shape[1])
    y_slice = np.arange(0, img.shape[0])

    x_mesh, y_mesh = np.meshgrid(x_slice, y_slice)

    ramp = x_mesh - y_mesh

    B = (ramp - np.min(ramp)) / (np.max(ramp) - np.min(ramp))

    gaussian_filter = matlab_style_gauss2D((9, 9), 2)
    max_blur = convolve(img, gaussian_filter, mode='nearest')

    spatial_vary_blur = (1 - B) * img + B * max_blur

    return spatial_vary_blur
