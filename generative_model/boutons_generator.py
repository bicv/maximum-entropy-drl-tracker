# Author: Tianhong Dai

import numpy as np
import math
import copy
import random
from generative_model.utils import distance_calculation
from generative_model.utils import varying_intensity_with_distance
from generative_model.utils import get_axon_gt_points
from generative_model.utils import pix_distance_to_axon


"""
This module is used to integrate the functions that used to
generate the boutons in the axon images

"""

# define the function that used to get the boutons
# TODO: in this function, i just implement the 15 parameters version...
# will add complete version in the future....
def get_boutons(axons_patch, axons_gt_points, variations, min_nbouton, max_nbouton, 
                min_bou_brightness, max_bou_brightness, bou_sigma, height, width, thickness, 
                sigma_spread, sigma_noise_axon, info_gt_points, min_dist_between_boutons):

    """
    this function will return the images with the synaptic boutons...
    more information will be added in the future!
    
    """
    # get the number of boutons
    num_boutons = random.randint(min_nbouton, max_nbouton)
    # get the bouton informations...
    boutons_info = get_info_boutons(height, width, axons_gt_points, variations, num_boutons, \
            min_bou_brightness, max_bou_brightness, thickness, info_gt_points, min_dist_between_boutons)
    for bou_idx in range(num_boutons):
        # draw the boutons...
        bouton_dist, row_inf, row_sup, col_inf, col_sup = draw_boutons(boutons_info, bou_sigma, height, width, bou_idx)
        # add it to the batch...
        axons_patch[row_inf:row_sup+1, col_inf:col_sup+1] = np.maximum(axons_patch[row_inf:row_sup+1, col_inf:col_sup+1], bouton_dist)
        if boutons_info[0][bou_idx] == 1:
            # if the type of the bouton is 1... do something special...
            terminal_branch, top_left, bottom_right = get_terminal_branch(boutons_info, sigma_spread, sigma_noise_axon, bou_idx)
            # convert the coordinate as int...
            axons_patch[top_left[0]:bottom_right[0]+1, top_left[1]:bottom_right[1]+1] = \
                    np.maximum(axons_patch[top_left[0]:bottom_right[0]+1, top_left[1]:bottom_right[1]+1], terminal_branch)

    return axons_patch


# get the boutons informations...
def get_info_boutons(height, width, axons_gt_points, variations, num_boutons, 
        min_bou_brightness, max_bou_brightness, thickness, info_gt_points, min_dist_between_boutons):
    
    """
    this function will get the information of the boutons
    which will be used to draw the circles representing synatic
    boutons: center, radius and brightness.

    """
    centers = None
    # types of bouton
    # TODO: Do something which make people could decide which kind of bouton is needed...
    boutons_info_1 = np.random.randint(0, 1, (num_boutons, ))
    # define the bouton_info_2...
    boutons_info_2 = np.zeros((num_boutons, 2))
    # define the bouton_info_7
    boutons_info_7 = np.zeros((num_boutons, 4))
    # define the bouton_info_8
    boutons_info_8 = np.zeros((num_boutons, 2))
    # define the bouton_info_9
    boutons_info_9 = np.zeros((num_boutons, 4))
    # define the bouton_info_10
    boutons_info_10 = np.zeros((num_boutons, 2))
    # define the bouton_info_11
    boutons_info_11 = np.zeros((num_boutons, 2))
    # define the bouton_info_4 and 5
    boutons_info_4 = None
    boutons_info_5 = None


    points = np.zeros((num_boutons, ))

    # get center,  and affiliated information in the case of a tb
    for bou in range(num_boutons):
        # define the type of the boutons.... normal bouton and terminal bouton(tb)
        if boutons_info_1[bou] == 1:
            # set the FLAGs..
            BOUTON_OK = True
            # until the center is in the image range...
            while BOUTON_OK:
                # random select the id of the gt points...(exclude the start and end points)
                idx = random.randint(1, axons_gt_points.shape[0]-2)
                # store this id...
                points[bou] = idx
                # store the points...
                point_temp = copy.deepcopy(axons_gt_points[idx, :])
                # thickness of the corresponding axons...
                thi = np.floor(thickness[int(info_gt_points[2, idx])]) + 1
                # the distance between the gt point and center of the tb bouton
                rho = thi * (2.1 + random.random())
                # min rotation angle possible
                theta_min = math.asin(2 * thi / rho)
                # pick rotation angle
                theta = theta_min + (np.pi - 2 * theta_min) * random.random()
                # pick one of the two neighbour gt points...
                next_point = get_next_point(axons_gt_points, idx, info_gt_points)
                # rotation matrix
                rotation_matrix = np.array([[math.cos(theta), -math.sin(theta)], \
                                        [math.sin(theta), math.cos(theta)]])

                # get the center of the tb bouton
                new_center = rho / np.linalg.norm(next_point - point_temp) * np.dot(rotation_matrix, \
                                                                    (next_point - point_temp).reshape((2, 1)))
                new_center = np.round(point_temp + new_center.squeeze())

                # check if the bouton is far enough from the existing ones...
                if bou == 0 or np.min(distance_calculation(centers[:,1], centers[:,0], new_center)) > \
                        min_dist_between_boutons:
                    if new_center[0] >= 0 and new_center[0] <= height-1 and new_center[1] >=0 and new_center[1] <= width-1:
                        boutons_info_2[bou, :] = new_center
                        if centers is None:
                            centers = np.expand_dims(new_center, 0)
                        else:
                            # stack the centers...
                            centers = np.vstack((centers, new_center))
                        # start to process the bouton information 7, store the two points used to calculate the center
                        boutons_info_7[bou, :] = np.concatenate((point_temp, next_point))
                        # store the two points which used to calculate the center...
                        boutons_info_8[bou, :] = np.array([variations[idx], thi])
                            
                        # extract new frame defined by GTPoint and center of the bouton
                        top_left = np.array([max(0, math.floor(min(point_temp[0], new_center[0]))), \
                                max(0, math.floor(min(point_temp[1], new_center[1])))])

                        bottom_right = np.array([min(height-1, math.ceil(max(point_temp[0], new_center[0]))), \
                                min(width-1, math.ceil(max(point_temp[1], new_center[1])))])

                        new_height = bottom_right[0] - top_left[0] + 1
                        new_width = bottom_right[1] - top_left[1] + 1

                        # pick a point between the gt point and the center of the bouton
                        inter_point = np.array([(new_height-1)*random.random(), (new_width-1)*random.random()])
                        boutons_info_9[bou, :] = np.concatenate((top_left, bottom_right))
                        boutons_info_10[bou, :] = inter_point
                        boutons_info_11[bou, :] = np.array([new_height, new_width])

                        BOUTON_OK = False
        
        else:
            # if it was not a tb bouton
            # set the new FLAG
            CENTER_OK = True
            while CENTER_OK:
                # redraw the center if it's too close to the other centers...
                # select the index...
                idx = random.randint(0, axons_gt_points.shape[0]-1)
                new_center = np.round(axons_gt_points[idx, :])
                if bou == 0 or np.min(distance_calculation(centers[:,1], centers[:,0], new_center)) > \
                        min_dist_between_boutons:
                    CENTER_OK = False
                    
            points[bou] = idx
            boutons_info_2[bou, :] = new_center
            if centers is None:
                centers = np.expand_dims(new_center, 0)
            else:
                centers = np.vstack((centers, new_center))
                
    # process the radius...
    boutons_info_3 = np.floor(thickness[info_gt_points[2, points.astype(np.int)].astype(np.int)] + 1)
    # bouton brightness...
    moy = np.random.randint(min_bou_brightness+1, max_bou_brightness, (num_boutons, )) / 100
    boutons_info_6 = moy
    
    # collect the bouton information
    boutons_info = (boutons_info_1, boutons_info_2, boutons_info_3, boutons_info_4, boutons_info_5, \
                boutons_info_6, boutons_info_7, boutons_info_8, boutons_info_9, boutons_info_10, boutons_info_11)

    return boutons_info
            

# get next points...
def get_next_point(points, idx_point, info_gt_points):
    """
    this function is used to get the next neighbour
    points

    """
    
    up_or_down = random.randint(0, 1)

    if up_or_down == 0:
        if info_gt_points[2, idx_point] == info_gt_points[2, idx_point + 1]:
            next_point = points[idx_point + 1, :]
        else:
            next_point = points[idx_point - 1, :]

    elif up_or_down == 1:
        if info_gt_points[2, idx_point] == info_gt_points[2, idx_point + 1]:
            next_point = points[idx_point - 1, :]
        else:
            next_point = points[idx_point + 1, :]
    
    return next_point


# define the function that draw the boutons
def draw_boutons(boutons_info, noise, height, width, bou_idx):
    """
    This function draws a cricle, it's center, radius and brightness
    being specified in the info, .....

    """
    center = boutons_info[1][bou_idx, :]
    radius = boutons_info[2][bou_idx]
    brightness = boutons_info[5][bou_idx]
    

    # it was in the middle of the images...
    if center[0] - radius >= 0 and center[0] + radius <= height - 1 and center[1] - radius >= 0 \
            and center[1] + radius <= width - 1:
        row_inf = center[0] - radius
        row_sup = center[0] + radius

        col_inf = center[1] - radius
        col_sup = center[1] + radius

        ref_row = radius + 1
        ref_col = radius + 1

    # top left...
    elif center[0] - radius < 0 and center[1] - radius < 0:
        row_inf = 0
        row_sup = center[0] + radius

        col_inf = 0
        col_sup = center[1] + radius

        ref_row = center[0]
        ref_col = center[1]

    # bottom left
    elif center[0] + radius >= height and center[1] - radius < 0:
        row_inf = center[0] - radius
        row_sup = height - 1

        col_inf = 0
        col_sup = center[1] + radius

        ref_row = radius + 1
        ref_col = center[1]

    # top right
    elif center[0] - radius < 0 and center[1] + radius >= width - 1:
        row_inf = 0
        row_sup = center[0] + radius

        col_inf = center[1] - radius
        col_sup = width - 1
        
        ref_row = center[0]
        ref_col = radius + 1
    
    # bottom right
    elif center[0] + radius >= height and center[1] + radius >= width:
        row_inf = center[0] - radius
        row_sup = height - 1

        col_inf = center[1] - radius
        col_sup = width - 1

        ref_row = radius + 1
        ref_col = radius + 1
    
    # top
    elif center[0] - radius < 0:
        row_inf = 0
        row_sup = center[0] + radius

        col_inf = center[1] - radius
        col_sup = center[1] + radius

        ref_row = center[0]
        ref_col = radius + 1

    # bottom
    elif center[0] + radius >= height:
        row_inf = center[0] - radius
        row_sup = height - 1

        col_inf = center[1] - radius
        col_sup = center[1] + radius

        ref_row = radius + 1
        ref_col = radius + 1
    
    # left
    elif center[1] - radius < 0:
        row_inf = center[0] - radius
        row_sup = center[0] + radius

        col_inf = 0
        col_sup = center[1] + radius

        ref_row = radius + 1
        ref_col = center[1]
    
    # right
    elif center[1] + radius >= width:
        row_inf = center[0] - radius
        row_sup = center[0] + radius

        col_inf = center[1] - radius
        col_sup = width - 1
        
        # center point of the bouton..
        ref_row = radius + 1
        ref_col = radius + 1
    
    else:
        print('still has some missing case!!!!')

    # compute the slice...
    x_slice = np.arange(0, col_sup - col_inf + 1)
    y_slice = np.arange(0, row_sup - row_inf + 1)

    # calculate the mesh grid...
    x_mesh, y_mesh = np.meshgrid(x_slice, y_slice)
    # get the distance to the center
    bouton_dist = np.sqrt((y_mesh - ref_row)**2 + (x_mesh - ref_col)**2)
    # std used for the gaussian profile...
    std = np.sqrt(-radius**2 / (2*np.log(0.05 / brightness)))
    
    point_info = np.where(bouton_dist >= radius)

    # set the point which large than radius as 0...
    for i in range(point_info[0].shape[0]):
        bouton_dist[point_info[0][i], point_info[1][i]] = np.inf
    
    # add some noise...
    bouton_dist = bouton_dist + noise * np.random.randn(bouton_dist.shape[0], bouton_dist.shape[1])
    # get the intensity of the bouton
    bouton_dist = varying_intensity_with_distance(bouton_dist, 'circle', 'gauss', std, 0, brightness)
    
    return bouton_dist, int(row_inf), int(row_sup), int(col_inf), int(col_sup)

# define the terminal branch...
def get_terminal_branch(boutons_info, sigma_spread, sigma_noise_axon, bou_idx):
    """
    This function draws the link between an axon branch and a terminal bouton(i know what it
    is, but i don't know why call it termial bouton haha, Know Nothing about Biomedical images)

    """
    top_left = boutons_info[8][bou_idx, 0:2]
    bottom_right = boutons_info[8][bou_idx, 2:4]

    height = boutons_info[10][bou_idx, 0]
    width = boutons_info[10][bou_idx, 1]

    # shift it to get top left corner
    new_gt_point = np.array([boutons_info[6][bou_idx, 0]-top_left[0]+1, boutons_info[6][bou_idx, 1]-top_left[1]+1])
    new_center = np.array([boutons_info[1][bou_idx, 0]-top_left[0]+1, boutons_info[1][bou_idx, 1]-top_left[1]+1])

    # point between the bouton center...
    inter_point = boutons_info[9][bou_idx]

    # define the intermedia points...
    middle_gt = (inter_point + new_gt_point) / 2
    middle_center = (inter_point + new_center) / 2
    control_points = np.vstack((new_gt_point, middle_gt, inter_point, middle_center, new_center))

    # fit a spline points...
    terminal_gt_points = get_axon_gt_points(control_points, 20)

    # get distance matrix...
    variation = boutons_info[7][bou_idx, 0] * np.ones((terminal_gt_points.shape[0], ))
    thick = boutons_info[7][bou_idx, 1] / 2
    terminal_dist, terminal_variations, _ = pix_distance_to_axon(int(height), int(width), terminal_gt_points, thick, variation)
    terminal_variations = terminal_variations + sigma_noise_axon * np.random.randn(int(height), int(width))

    # convert it to intensity matrix
    terminal_variations[terminal_variations == np.inf] = 0
    terminal_branch = varying_intensity_with_distance(terminal_dist, 'axons', 'gauss', sigma_spread*0.75, terminal_variations, 5)

    return terminal_branch, top_left.astype(np.int), bottom_right.astype(np.int)
