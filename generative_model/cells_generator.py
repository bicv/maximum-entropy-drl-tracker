# Author: Tianhong Dai

import numpy as np
import random
import copy
from generative_model.utils import matlab_style_gauss2D
from generative_model.utils import varying_intensity_with_distance 
from scipy.ndimage.filters import convolve


"""
This module will implement the main funtion of generate the cell
and some other assistant function which will help the main function...

"""

# define the main functions to generate the cells...
def get_circles(axons_patch, height, width, sigma_noise_circle, min_ncircle, max_ncircle, \
        min_brightness_circle, max_birghtness_circle, min_radius, max_radius):
    """
    Will add more detail information

    """
    new_axon_patch = copy.deepcopy(axons_patch)
    num_circles = random.randint(min_ncircle, max_ncircle)

    # the main loop to generate the circles...
    for circle_idx in range(num_circles):
        # randomly pick parameters for the cells to draw...
        radius = random.randint(min_radius, max_radius)
        # TODO: what is the selection pixel?
        selection_pixel = random.randint(2, 4)
        # the brightness..
        brightness = random.randint(min_brightness_circle, max_birghtness_circle) / 100

        # draw the circles
        dist, row_inf, row_sup, col_inf, col_sup = draw_cells(axons_patch, radius, selection_pixel, \
                brightness, height, width, sigma_noise_circle)

        # put the cell in the images...
        new_axon_patch[row_inf:row_sup+1, col_inf:col_sup+1] = np.maximum(new_axon_patch[row_inf:row_sup+1, col_inf:col_sup+1], \
                            dist)

    return new_axon_patch

# draw the circles...
def draw_cells(axons_patch, radius, selection_pixel, brightness, height, width, sigma_noise_circle):
    """
    this function will draw the cells on the origin images

    """
    cradius = np.ceil(radius / 2)
    h = radius + cradius

    # get all the locations of the noise source
    num_source = 5
    row_noise_source = np.array([h+1])
    row_noise_source = np.concatenate((row_noise_source, h+1+np.random.randint(-cradius+1, cradius, (num_source-1))))
    
    col_noise_source = np.array([h+1])
    col_noise_source = np.concatenate((col_noise_source, h+1+np.random.randint(-cradius+1, cradius, (num_source-1))))

    # sigma to make gaussian profile reach 5% of its peak at the radius
    std = np.sqrt(-radius**2 / (2 * np.log(0.05 * brightness)))

    # gets the pixel distances to the center of the cell and define points out...
    dist = np.ones((int(2*h + 1), int(2*h + 1))) * np.inf
    x_slice = np.arange(0, 2*h+1)
    y_slice = np.arange(0, 2*h+1)
    
    # get the mesh grid...
    x_mesh, y_mesh = np.meshgrid(x_slice, y_slice)
    for i in range(num_source):
        dist = np.minimum(dist, np.sqrt((x_mesh - col_noise_source[i])**2 + (y_mesh - row_noise_source[i])**2))

    dist[dist > radius] = np.inf

    # convolve the obtained matrix with gaussian mask... to deform the images..
    mask_size = np.ceil(radius * 0.5)
    g2 = matlab_style_gauss2D((mask_size, mask_size), std)
    dist = convolve(dist, g2, mode='nearest')

    # add noise to the matrix by resetting randomly picked pixel to radius dist
    selection_matrix = np.random.randint(1, selection_pixel+1, (int(2*h+1), int(2*h+1)))
    dist[(selection_matrix == selection_pixel) * (dist < np.inf)] = radius
    
    # add white noise to the circle...
    dist = dist + sigma_noise_circle * np.random.randn(int(2*h+1), int(2*h+1))

    # gets intensity out of distance...
    dist = varying_intensity_with_distance(dist, 'circle', 'gauss', std, 0, brightness) 

    # remove the zero rows and columns..
    # TODO: make a more efficient one....
    # firstly remove the col one
    col_num = dist.shape[1]
    row_num = dist.shape[0]

    rm_col_idx = []
    rm_row_idx = []
    
    # start..
    for idx in range(col_num):
        info = np.nonzero(dist[:, idx])
        if info[0].shape[0] == 0:
            rm_col_idx.append(idx)

    dist = np.delete(dist, rm_col_idx, axis=1)
    
    # then remove the row...
    for idx in range(row_num):
        info = np.nonzero(dist[idx, :])
        if info[0].shape[0] == 0:
            rm_row_idx.append(idx)

    dist = np.delete(dist, rm_row_idx, axis=0)

    # set the FLAG...
    CROSS = True

    while CROSS:
        # top right corner of the circle
        topr = np.array([random.randint(-np.ceil(cradius), height-1), random.randint(-np.ceil(cradius), width-1)])
        
        # boundaries of the insetred cell
        row_inf = int(max(topr[0], 0))
        row_sup = int(min(topr[0] + dist.shape[0] - 1, height - 1))

        col_inf = int(max(topr[1], 0))
        col_sup = int(min(topr[1] + dist.shape[1] - 1, width - 1))

        # binary mask to check overlapping between axons and cell...
        axon_mask = axons_patch[row_inf:row_sup+1, col_inf:col_sup+1]
        axon_mask = axon_mask > 0 
        cell_mask = dist > 0

        # crop the cell parts that are out side the images...
        x_slice = np.arange(topr[1], topr[1] + dist.shape[1])
        y_slice = np.arange(topr[0], topr[0] + dist.shape[0])

        x_mesh, y_mesh = np.meshgrid(x_slice, y_slice)

        # rm the cols and rows....
        # TODO: So urgly....
        rm_row_idx = []
        num_test = y_mesh.shape[0]
        for idx in range(num_test):
            if y_mesh[idx, 0] < 0 or y_mesh[idx, 0] > height - 1:
                rm_row_idx.append(idx)
        
        cell_mask = np.delete(cell_mask, rm_row_idx, axis=0)

        rm_col_idx = []
        num_test = x_mesh.shape[1]
        for idx in range(num_test):
            if x_mesh[0, idx] < 0 or x_mesh[0, idx] > width - 1:
                rm_col_idx.append(idx)

        cell_mask = np.delete(cell_mask, rm_col_idx, axis=1)

        # check any overlapping between axons and cell
        #axon_mask = axon_mask.astype(np.int)
        #cell_mask = cell_mask.astype(np.int)
        #if np.max(axon_mask + cell_mask) < 2:
        dist = np.delete(dist, rm_row_idx, axis=0)
        dist = np.delete(dist, rm_col_idx, axis=1)
        CROSS = False

    return dist, row_inf, row_sup, col_inf, col_sup 
