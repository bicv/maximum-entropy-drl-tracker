""" This code define the main body of the axon generator -- python version
    Date: 2018-02-14
    Author: Tianhong
    Modified by Shafa Balaram to generate images using specified seed numbers for the
    maximum-entropy DRL neural tracker. """

import numpy as np
import random
import copy
from generative_model.utils import get_start_coords
from generative_model.utils import get_valid_direction
from generative_model.utils import get_axon_gt_points
from generative_model.utils import make_variations
from generative_model.utils import pix_distance_to_axon
from generative_model.utils import check_crossing
from generative_model.utils import varying_intensity_with_distance 
from generative_model.utils import linear_mapping
from generative_model.boutons_generator import get_boutons
from generative_model.cells_generator import get_circles
from generative_model.noise_generator import get_noise
from generative_model.blur_generator import get_blur

class axon_generator:
    def __init__(self, args):
        self.args = args

    # this function is used to generate the images
    def generate_imgs(self, seed_num):
        self.args.seed_num = seed_num
        random.seed(self.args.seed_num)
        np.random.seed(self.args.seed_num)
        # set some flags..
        RESTART = False
        while RESTART == False:
            # set the number of axons and branches
            num_axons = random.randint(self.args.min_axons, self.args.max_axons)
            num_branch = np.random.randint(self.args.min_bran, self.args.max_bran+1, (num_axons, ))
            # set the thickness per branch
            thickness = np.random.randint(round(self.args.min_thickness*100), round(self.args.max_thickness*100), (np.sum(num_branch), )) / 100
            # set the number of total points...
            total_points = np.sum(num_branch) * self.args.NSpline_points
            # set the axon ground truth points...
            axon_gt_points = np.zeros((total_points, 2))
            # vector of the axon intensity variations...
            variations = np.zeros((total_points, ))
            # vector of the axon intensity variations for the ground truth...
            variations_gt = np.zeros((total_points, ))
            # define the pointer to point the current branch
            pointer = 0
            pointer_axon = 0
            # distance to the axons ground truth points without gaps
            axons_dist = np.ones((self.args.height, self.args.width)) * np.inf
            # the intensity variations without gaps...
            axons_variations = np.ones((self.args.height, self.args.width)) * np.inf
            # the intensity variations 
            axons_gt_variations = np.ones((self.args.height, self.args.width)) * np.inf
            # define information matrix...
            info_gt_points = None
            gap_indices = None
            #print('start to create the axons!!!')
            for axon_id in range(num_axons):
                #print('start to create axon No.' + str(axon_id))
                # here is the mother branch of the axons..
                if axon_id > 0:
                    pointer_axon = pointer_axon + num_branch[axon_id - 1] * self.args.NSpline_points

                # set the number of cross points...
                num_cross = 0
                # set the FLAG...
                CROSS = True
                while CROSS:
                    if self.args.crossing:
                        start, dir_vec = get_start_coords(self.args.height, self.args.width)
                        CROSS = False 
                    else:
                        # if we don't allow the cross, we need to check if the random start point is already exist
                        # set the Flag
                        START_OK = False
                        while START_OK == False:
                            start, dir_vec = get_start_coords(self.args.height, self.args.width)
                            if axons_dist[start[0], start[1]] == np.inf:
                                START_OK = True

                    # start to do the next step..
                    control_points_temp = copy.deepcopy(start)
                    control_points = []
                    control_points.append(control_points_temp) 
                    # init the FLAGS for terminal
                    AT_TERMINAL_STATE = False

                    while AT_TERMINAL_STATE == False:
                        dir_vec = get_valid_direction(dir_vec, self.args.conformity)
                        control_points_temp = control_points_temp + self.args.step_size * dir_vec
                        # check if it reach the terminal
                        if control_points_temp[0] <= 0 or control_points_temp[0] >= self.args.height - 1\
                                or control_points_temp[1] <= 0 or control_points_temp[1] >= self.args.width - 1:
                            AT_TERMINAL_STATE = True
                            # if at the terminal process the last control points
                            control_points_temp[0] = min(control_points_temp[0], self.args.height - 1)
                            control_points_temp[0] = max(control_points_temp[0], 0)
                            control_points_temp[1] = min(control_points_temp[1], self.args.width - 1)
                            control_points_temp[1] = max(control_points_temp[1], 0)
                            
                        # append the control points to the collections
                        control_points.append(control_points_temp)
                    
                    # convert it into a numpy array... easy to compute
                    control_points = np.array(control_points)
                    # create a spline going through all of the control points...
                    interpolation_point = get_axon_gt_points(control_points, self.args.NSpline_points)
                    axon_gt_points[pointer*self.args.NSpline_points:pointer*self.args.NSpline_points\
                            +self.args.NSpline_points, :] = interpolation_point
                    # fill in the variations for this branch
                    # TODO: revised the axon intensity value...
                    variation_temp, variation_gt_temp, gap_index = make_variations(self.args.min_axons_intensity/100+\
                            0.01+(self.args.max_axons_intensity/100-self.args.min_axons_intensity/100-0.02)*random.random(), 
                            self.args.axon_profile, 
                            self.args.NSpline_points, 
                            self.args.min_axons_intensity, 
                            self.args.max_axons_intensity, 
                            self.args.min_period, 
                            self.args.max_period, 
                            self.args.min_gap_size, 
                            self.args.max_gap_size)

                    # assign the variation to the collections...
                    variations[pointer*self.args.NSpline_points:pointer*self.args.NSpline_points+self.args.NSpline_points] = variation_temp
                    variations_gt[pointer*self.args.NSpline_points:pointer*self.args.NSpline_points+self.args.NSpline_points] = variation_gt_temp 
                    # get the distance to the GT points of the pixels belonging to the spline
                    branch_dist, branch_variations, branch_gt_variations = pix_distance_to_axon(self.args.width, self.args.height, 
                            axon_gt_points[pointer*self.args.NSpline_points:self.args.NSpline_points+pointer*self.args.NSpline_points, :],
                            thickness[pointer],
                            variations[pointer*self.args.NSpline_points:self.args.NSpline_points+pointer*self.args.NSpline_points],
                            variations_gt[pointer*self.args.NSpline_points:self.args.NSpline_points+pointer*self.args.NSpline_points]
                            ) 
                    
                    # now we check if the spline crosses existing axons...
                    # find if they has some cross section...
                    # TODO: Maybe ugly?
                    branch_dist_copy = copy.deepcopy(branch_dist)
                    axons_dist_copy = copy.deepcopy(axons_dist)

                    branch_dist_copy[branch_dist_copy == np.inf] = 0
                    axons_dist_copy[axons_dist_copy == np.inf] = 0
                    
                    mask = branch_dist_copy * axons_dist_copy
                    
                    info = np.where(mask != 0)

                    # start to check
                    if self.args.crossing == False and info[0].shape[0] == 0:
                        CROSS = False
                    else:
                        num_cross += 1

                    if num_cross > 60:
                        RESTART = True

                    if RESTART: break

                if RESTART: break 

                # update the matrix with the new branch...
                axons_dist = np.minimum(branch_dist, axons_dist)
                axons_variations = np.minimum(branch_variations, axons_variations)
                axons_gt_variations = np.minimum(branch_gt_variations, axons_gt_variations)
                
                # TODO: implement the update info and axons_gt_points with gap...
                if info_gt_points is None:
                    info_cat_temp1 = (axon_id + 1) * np.ones((1, self.args.NSpline_points))
                    info_cat_temp2 = np.ones((1, self.args.NSpline_points))
                    info_cat_temp3 = pointer * np.ones((1, self.args.NSpline_points))
                    # concatenate them together
                    info_gt_points = np.concatenate((info_cat_temp1, info_cat_temp2, info_cat_temp3), 0)
                else:
                    # here repeat... lazy to revised...
                    info_cat_temp1 = (axon_id + 1) * np.ones((1, self.args.NSpline_points))
                    info_cat_temp2 = np.ones((1, self.args.NSpline_points))
                    info_cat_temp3 = pointer * np.ones((1, self.args.NSpline_points))
                    info_cat_temp = np.concatenate((info_cat_temp1, info_cat_temp2, info_cat_temp3), 0)
                    # concatenate...
                    info_gt_points = np.concatenate((info_gt_points, info_cat_temp), 1)

                # process the gap indices...
                if gap_indices is None:
                    gap_indices = gap_index
                else:
                    gap_indices = np.concatenate((gap_indices, gap_index))


                # update the pointer
                pointer += 1

                # in here we will update the ----- daughter branch of the axon -----------------------
                """
                The code below is about the daughter branch of the axons...

                """
                for branch_id in range(num_branch[axon_id] - 1):
                    print('start to create the axon No.' + str(axon_id) + '\'s branch No.' + str(branch_id+1))
                    # set the flag...
                    CROSS = True
                    while CROSS:
                        START_OK = True
                        while START_OK:
                            # add 1 here... avoid the bug...
                            daughter_start = random.randint(pointer_axon+1, pointer_axon + self.args.NSpline_points * (branch_id+1))
                            if daughter_start not in gap_indices:
                                START_OK = False

                        # define the start point of the daughter points...
                        control_points_temp = copy.deepcopy(axon_gt_points[daughter_start, :])
                        control_points = []
                        # store the control points...
                        control_points.append(control_points_temp)
                        # set the initial direction vector
                        dir_vec = axon_gt_points[daughter_start, :] - axon_gt_points[daughter_start-1, :]
                        # normalize the vector...
                        dir_vec = dir_vec / np.linalg.norm(dir_vec)
                        # set another FLAG...
                        AT_TERMINAL_STATE = False
                        while AT_TERMINAL_STATE == False:
                            # for the initial direction, it should be around 45 degree
                            # TODO: is this should be modified?
                            if len(control_points) == 1:
                                dir_vec = get_valid_direction(dir_vec, np.sqrt(2)/2)
                            else:
                                dir_vec = get_valid_direction(dir_vec, self.args.conformity)

                            control_points_temp = control_points_temp + self.args.step_size * dir_vec

                            # check if the control points is suitable...
                            if control_points_temp[0] <= 0 or control_points_temp[0] >= self.args.height-1\
                                    or control_points_temp[1] <= 0 or control_points_temp[1] >= self.args.width-1:

                                AT_TERMINAL_STATE = True
                                # adjust the control points temp if needed...
                                control_points_temp[0] = min(control_points_temp[0], self.args.height - 1)
                                control_points_temp[0] = max(control_points_temp[0], 0)
                                control_points_temp[1] = min(control_points_temp[1], self.args.width - 1)
                                control_points_temp[1] = max(control_points_temp[1], 0)
                            # start to append the points..    
                            control_points.append(control_points_temp)

                        # start to the next step...
                        # convert the list into numpy array...
                        control_points = np.array(control_points)
                        # get the interpolation points...
                        interpolation_point = get_axon_gt_points(control_points, self.args.NSpline_points)
                        axon_gt_points[pointer*self.args.NSpline_points:pointer*self.args.NSpline_points\
                                +self.args.NSpline_points, :] = interpolation_point
                        # fill in the variations for this branch
                        variation_temp, variation_gt_temp, gap_index = make_variations(variations[daughter_start],\
                                self.args.branch_profile, 
                                self.args.NSpline_points, 
                                self.args.min_axons_intensity, 
                                self.args.max_axons_intensity, 
                                self.args.min_period, 
                                self.args.max_period, 
                                self.args.min_gap_size, 
                                self.args.max_gap_size)

                        # assign the variation to the collections...
                        variations[pointer*self.args.NSpline_points:pointer*self.args.NSpline_points+self.args.NSpline_points] = variation_temp
                        variations_gt[pointer*self.args.NSpline_points:pointer*self.args.NSpline_points+\
                                                                                            self.args.NSpline_points] = variation_gt_temp
                        # get the distance to the GT points of the pixels belonging to the spline
                        branch_dist, branch_variations, branch_gt_variations = pix_distance_to_axon(self.args.width, self.args.height, 
                                axon_gt_points[pointer*self.args.NSpline_points:self.args.NSpline_points+pointer*self.args.NSpline_points, :],
                                thickness[pointer],
                                variations[pointer*self.args.NSpline_points:self.args.NSpline_points+pointer*self.args.NSpline_points],
                                variations_gt[pointer*self.args.NSpline_points:self.args.NSpline_points+pointer*self.args.NSpline_points]
                                )

                        # check if the axons allowed crossing...
                        if self.args.crossing:
                            # set the FLAG
                            CROSS = False
                        else:
                            # we need to make sure the daughter branch will not cross the mother branch...
                            # a given distance from the branching point
                            CROSS = check_crossing(axons_dist, branch_dist, self.args.straight_branching*thickness[pointer], 
                                    control_points[0, :])
                            
                            if CROSS:
                                num_cross += 1
                        if num_cross > 60:
                            RESTART = True
                        if RESTART == True: break
                    if RESTART == True: break
                    # update the matrix with the new branch...
                    axons_dist = np.minimum(axons_dist, branch_dist)
                    axons_variations = np.minimum(axons_variations, branch_variations)
                    axons_gt_variations = np.minimum(axons_gt_variations, branch_gt_variations)
                    
                    # start to process the ground truth information...
                    info_cat_temp1 = (axon_id + 1) * np.ones((1, self.args.NSpline_points))
                    info_cat_temp2 = (branch_id + 1) * np.ones((1, self.args.NSpline_points))
                    info_cat_temp3 = pointer * np.ones((1, self.args.NSpline_points))
                    info_cat_temp = np.concatenate((info_cat_temp1, info_cat_temp2, info_cat_temp3), 0)

                    # concatenate...
                    info_gt_points = np.concatenate((info_gt_points, info_cat_temp), 1)

                    # process the gap indices...
                    gap_indices = np.concatenate((gap_indices, gap_index))

                    pointer += 1
                
                if RESTART: break
            if RESTART:
                RESTART = False
                continue
            else:
                RESTART = True 
        
        # TODO: Need to debug the indice here!!!
        #print(gap_indices)   
        # start to do the post-processing...
        # obtain the images from the distance matrix.....
        axons_variations[axons_variations == np.inf] = 0
        axons_gt_variations[axons_gt_variations == np.inf] = 0
        # add noise to the variations...
        axons_variations = axons_variations + self.args.sigma_noise_axons * np.random.randn(self.args.height, self.args.width)
        # transform the distance into intensity, taking the variations into account
        # TODO: Maybe add these things into the arguments..and the brightness things?!!?
        axons_patch = varying_intensity_with_distance(axons_dist, 'axons', 'gauss', 
                                                        self.args.sigma_spread, axons_variations, 5)
        
        axons_gt_patch = varying_intensity_with_distance(axons_dist, 'axons', 'gauss', 0.7, axons_gt_variations, 5)
        #axons_patch = np.abs(axons_patch)
        # adding the buotons...
        """
        Here we will add the boutons...
        
        """
        # copy the axons points with gaps...
        axon_gt_points_with_gaps = copy.deepcopy(axon_gt_points)
        axon_gt_points_with_gaps = np.delete(axon_gt_points_with_gaps, gap_indices, 0)
        
        # copy the variations
        variations_with_gaps = copy.deepcopy(variations)
        variations_with_gaps = np.delete(variations_with_gaps, gap_indices)

        info_gt_points_with_gaps = copy.deepcopy(info_gt_points)
        info_gt_points_with_gaps = np.delete(info_gt_points_with_gaps, gap_indices, 1)
        
        # get the boutons...
#         print(self.args.add_bouton)
        if self.args.add_bouton:
            axons_patch = get_boutons(axons_patch, axon_gt_points_with_gaps, variations_with_gaps, self.args.min_nbouton, \
                self.args.max_nbouton, self.args.min_brightness_bouton, self.args.max_brightness_bouton, self.args.sigma_noise_bouton, \
                self.args.height, self.args.width, thickness, self.args.sigma_spread, self.args.sigma_noise_axons, \
                info_gt_points_with_gaps, self.args.min_dis_bet_bouton)
        
        #axons_patch = np.abs(axons_patch)
        
        """
        Here we will start to add the circles...

        """
        # get circles..
        if self.args.add_circle:
            axons_patch = get_circles(axons_patch, self.args.height, self.args.width, self.args.sigma_noise_circle, \
                    self.args.min_ncircle, self.args.max_ncircle, self.args.min_brightness_circle, self.args.max_brightness_circle, \
                    self.args.min_radius, self.args.max_radius)
        
        """
        Here we will start to add the noise...

        """
        # get noise...
        sigma_noise = random.randint(round(self.args.sigma_noise_min*1e5), round(self.args.sigma_noise_max*1e5))/1e5
        noise_lambda = random.randint(self.args.lambda_min, self.args.lambda_max)
        axons_patch = get_noise(axons_patch, sigma_noise, noise_lambda, self.args.height, self.args.width)

        # convert the images into the gray_scale..
        axons_patch = np.floor(axons_patch * self.args.max_intensity / np.max(axons_patch))
        axons_patch = linear_mapping(axons_patch)
        axons_patch = get_blur(axons_patch)
        
        # generate the ground truth...
        axons_gt_patch = np.floor(axons_gt_patch * self.args.max_intensity / np.max(axons_gt_patch)) / 255
        #axons_gt_patch = linear_mapping(axons_gt_patch)

        # here is the output for the RL-training environment...
        return axons_patch, axons_gt_patch, axon_gt_points
      
