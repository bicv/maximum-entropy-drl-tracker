# Author: Tianhong Dai

import numpy as np
from generative_model.utils import matlab_style_gauss2D
from scipy.ndimage.filters import convolve
"""
This module is used to generate the noise

"""

def get_noise(patch, sigma_noise, noise_lambda, height, width):
    """
    generate the noise and add them to the axon images...


    """
    sigma_1 = 0.1
    sigma_2 = 6

    size_1 = 2
    size_2 = 50

    patch = np.floor(patch * 255 / np.max(patch))

    poisson = np.random.poisson(noise_lambda, size=(height, width))
    gauss = sigma_noise * np.random.randn(height, width)
    oblur_1 = matlab_style_gauss2D((size_1, size_1), sigma_1) 
    oblur_2 = matlab_style_gauss2D((size_2, size_2), sigma_2) * 25
    patch = convolve(patch, oblur_1, mode='nearest') + convolve(poisson, oblur_2, mode='nearest') + gauss

    patch[patch < 0] = 0
    patch = np.floor(patch*255/np.max(patch))

    return patch
