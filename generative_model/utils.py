# Author: Tianhong Dai

import numpy as np
import random
from scipy import interpolate
import math
import copy
"""
This module contains some useful function which could be used in
the generator..

"""

# get the start points of the axon..
def get_start_coords(height, width):
    """
    random generate a start point on one of the four edges
    
    the start points should be [y, x]

    the initial direction vector should be [y, x]

    """
    which_side = random.randint(1, 4)
    
    # from the top side...
    if which_side == 1:
        start = np.array([0, random.randint(round(width/4), round(3*width/4))])
        dir_vec = np.array([1, 0])
    
    # from the left side of the image
    elif which_side == 2:
        start = np.array([random.randint(round(height/4), round(3*height/4)), 0])
        dir_vec = np.array([0, 1])
    
    # from the bottom side of the images
    elif which_side == 3:
        start = np.array([height - 1, random.randint(round(width/4), round(3*width/4))])
        dir_vec = np.array([-1, 0])

    # from the right side of the images
    elif which_side == 4:
        start = np.array([random.randint(round(height/4), round(3*height/4)), width - 1])
        dir_vec = np.array([0, -1])

    return start, dir_vec
        

# need to get the valiadtion
def get_valid_direction(dir_vec, conformity):
    """
    get a valid direction that for the random walk
    
    conformity: this decide how curve is your axons, increase for straight line

    decrease for more curve line.

    """
    # normalize the direction vector...(I think this is useless, remove in future)
    dir_vec = dir_vec / np.linalg.norm(dir_vec)
    
    # set the FLAG
    NO_VALID_DIRECTION = True
    while NO_VALID_DIRECTION:
        # generate random direction vector..
        random_dir = np.random.randn(2)
        random_dir = random_dir / np.linalg.norm(random_dir)
        # calculate the coise of the angle between the random vector and current vector
        dot_product = np.dot(dir_vec, random_dir)

        if dot_product > conformity:
            NO_VALID_DIRECTION = False

    return random_dir 


# generate the interpolation points...
def get_axon_gt_points(control_points, num_points):
    """
    This function will generate the interpolation point according to the 
    control point

    """
    temp = np.linspace(0, 1, control_points.shape[0])
    interpolate_function = interpolate.CubicSpline(temp, control_points)
    gt_points = interpolate_function(np.linspace(0, 1, num_points))

    return gt_points
    

# make variations
def make_variations(start_variation, profile_type, num_points, min_axon_intensity, max_axon_intensity, 
                        min_period, max_period, min_gap_size, max_gap_size):

    """
    This function will generate the vectors with value according to the given
    profile. 
    In actually, it should be the coefficient that for its intensity
    
    In the version of 
    
    """
    gap_index = []

    # the constant variations....
    if profile_type == 'constant':
        variation = start_variation * np.ones((num_points, ))

    # TODO: I need to write the cosine profile and the linear profile
    elif profile_type == 'cosine':
        num_period = min_period + (max_period - min_period) * random.random()
        max_intensity = start_variation + (max_axon_intensity - min(start_variation, max_axon_intensity-0.01)) * random.random()
        min_intensity = start_variation - (max(start_variation, min_axon_intensity+0.01) - min_axon_intensity) * random.random()
        while min_intensity >= max_intensity:
            intensities = min_axon_intensity + 0.01 + (max_axon_intensity - min_axon_intensity - 0.02) * np.random.random((2, ))
            max_intensity = np.max(intensities)
            min_intensity = np.min(intensities)
        a = 0.5 * (max_intensity + min_intensity)
        b = 0.5 * (max_intensity - min_intensity)
        
        phi = np.arccos(np.clip((round(start_variation)/100-a)/b, -1, 1))
        # up or down
        up_or_down = random.randint(0, 1)
        slice_point = np.linspace(0, 2*np.pi*num_period-1, num_points)
        if up_or_down == 0:
            variation = a + b * np.cos(slice_point - phi)
        else:
            variation = a + b * np.cos(slice_point + phi)
    
    # TODO: the above one has unknown bug!!!
    elif profile_type == 'my_cosine':
        # define the max period
        my_max_period = 20

        start_phase_list = [0, 0.5*np.pi, np.pi, 1.5*np.pi]
        start_phase = random.sample(start_phase_list, 1)[0]
        period = np.linspace(start_phase, start_phase + my_max_period, num_points)
        
        variation = 0.25 + 0.15 * np.cos(period)
        
        sel_pos = []
        for _ in range(2):
            rand_sel_pos = random.randint(0, num_points - 31)
            sel_pos.append(rand_sel_pos)
        for idx in range(len(sel_pos)):
            #variation[sel_pos[idx]:sel_pos[idx]+30] = np.random.normal(1.2, 0.1, 30)
            small_period = np.linspace(start_phase, start_phase+4, 10)
            variation[sel_pos[idx]:sel_pos[idx]+10] = 0.8 + 0.5 * np.cos(small_period)


    # Here, we need a copy of the ground truth copy's variations...
    # TODO: I set a defualt variations here, could be added into the arguments...
    default_variation = 0.2
    
    # build up the variations gt...
    variation_gt = default_variation * np.ones((num_points, ))

    if max_gap_size > 0:
        gap_size = random.randint(min_gap_size, max_gap_size)
        variation, gap_index = make_gaps(variation, gap_size)

    return variation, variation_gt, gap_index


# the function that make the gaps...
def make_gaps(variation, gap_size):
    """
    This function will select one of the points and set it to zero...

    """
    # select and idx that will be set as 0...
    idx = random.randint(0, variation.shape[0] - 1)

    if idx > 0 and idx < variation.shape[0] - 1:
        gap_index_start = max(idx - gap_size, 0)
        gap_index_end = min(idx + gap_size, variation.shape[0] - 1)
        

        #TODO: the code here has some problems? Redudant... Need to be revised in the future
        # modify the variations... before
        variation[max(idx - gap_size, 0):idx+1] = np.linspace(variation[max(idx - gap_size, 0)], 0, min(gap_size+1, idx+1))
        # modify the variations... after
        variation[idx:min(idx+gap_size+1, variation.shape[0])] = np.linspace(0, variation[min(idx+gap_size, variation.shape[0]-1)], 
                                        min(gap_size + 1, variation.shape[0]-idx))
    
    # if the index of the gap is 0...
    elif idx == 0:
        gap_index_start = 0
        gap_index_end = gap_size

        # modify the variations...
        variation[0:gap_size + 1] = np.linspace(0, variation[gap_size], gap_size+1)
    
    # if the index of the gap is the end of the variation
    elif idx == variation.shape[0] - 1:
        gap_index_start = variation.shape[0] - 1 - gap_size
        gap_index_end = variation.shape[0] - 1

        # modify the variation
        variation[variation.shape[0]-1-gap_size:variation.shape[0]] = np.linspace(variation[variation.shape[0]-1-gap_size], 0, gap_size+1)
    
    # covert the gap index into the numpy array...
    # gap_index = np.array([gap_index_start, gap_index_end])
    gap_index = np.arange(gap_index_start, gap_index_end+1)

    return variation, gap_index 


def pix_distance_to_axon(height, width, axon_gt_points, thickness, variations, *args):
    """
    This function is used to draw several branches, where the gaps have already defined
    set both min/max gap size as 0

    In the version for the axon tracking, the variations for the ground truth will be added... 

    """
    # create a matrix initialized with inf...
    branch_dist = np.ones((height, width)) * np.inf
    # create a variations matrix initialized with inf...
    branch_variations = np.ones((height, width)) * np.inf 
    # create a variations matrix initialized with inf...
    branch_gt_variations = np.ones((height, width)) * np.inf

    # check if receive the arguments...
    if len(args) != 0:
        variations_gt_copy = copy.deepcopy(args[0])
    

    # loop over the gt points out of the range...
    for idx in range(axon_gt_points.shape[0]):
        if axon_gt_points[idx, 0] >= 0 and axon_gt_points[idx, 0] <= height-1 and \
                            axon_gt_points[idx, 1] >= 0 and axon_gt_points[idx, 1] <= width-1:
            # compute the slice for build up the meshgrid...
            y_slice_start = max(0, math.floor(axon_gt_points[idx, 0] - thickness))
            y_slice_end = min(height-1, math.ceil(axon_gt_points[idx, 0] + thickness))
            y_slice = np.arange(y_slice_start, y_slice_end+1)

            x_slice_start = max(0, math.floor(axon_gt_points[idx, 1]-thickness))
            x_slice_end = min(width-1, math.ceil(axon_gt_points[idx, 1]+thickness))
            x_slice = np.arange(x_slice_start, x_slice_end+1)
            
            # calculate the meshgrid according to the slice...
            x_mesh, y_mesh = np.meshgrid(x_slice, y_slice)
            # calculate the distance...
            dist = distance_calculation(x_mesh, y_mesh, axon_gt_points[idx])

            # ravel the x_mesh and the y_mesh
            x_mesh = x_mesh.ravel()
            y_mesh = y_mesh.ravel()
            dist = dist.ravel()
            for pixel_id in range(dist.shape[0]):
                this_row = y_mesh[pixel_id]
                this_col = x_mesh[pixel_id]
                # check the minimual value...
                a = min(branch_dist[this_row, this_col], dist[pixel_id])
                if a == dist[pixel_id]:
                    branch_dist[this_row, this_col] = a
                    branch_variations[this_row, this_col] = variations[idx]
                    if len(args) != 0:
                        branch_gt_variations[this_row, this_col] = variations_gt_copy[idx]

    return branch_dist, branch_variations, branch_gt_variations

# calculate the distance 
def distance_calculation(x, y, gt_points):
    """
    This is just the assistant function to calculate
    the distance between the ground truth point and
    all of the other pixels

    """

    dist = np.sqrt((x - gt_points[1])**2 + (y - gt_points[0])**2)
    return dist 

# check the crossing problem....
def check_crossing(axons_dist_without_gap, branch_dist_without_gap, threshold, new_start):
    """
    This function is used to check the crossing problem of the 
    mother branch

    """
    # set the FLAG...
    CROSS = True
    # copy the things
    axons_dist_copy = copy.deepcopy(axons_dist_without_gap)
    branch_dist_copy = copy.deepcopy(branch_dist_without_gap)
    # make some masks
    axons_dist_copy[axons_dist_copy == np.inf] = 0
    branch_dist_copy[branch_dist_copy == np.inf] = 0
    # times the two mask and generate the resulting mask
    result_mask = axons_dist_copy * branch_dist_copy

    # start to check if there has some things
    info = np.where(result_mask != 0)

    if info[0].shape[0] == 0:
        # set the FLAG
        CROSS = False
    else:
        # get the number of pixels....
        num_pixel = info[0].shape[0]
        pixel_id = 0
        while CROSS:
            this_row = info[0][pixel_id]
            this_col = info[1][pixel_id]
            # calculate the distance from the start point to the control points
            dist_to_start_cp = np.sqrt((new_start[0] - this_row)**2 + (new_start[1] - this_col)**2)
            if dist_to_start_cp > threshold:
                break
            elif pixel_id == (num_pixel - 1):
                CROSS = False
            else:
                pixel_id += 1

    return CROSS

# convert the distance into the intesnity
def varying_intensity_with_distance(dist_matrix, structure_type, profile_type, sigma_spread,
        axons_variations, brightness):
    """
    This function will conver the distance matrix into intensity
    the current options include a Butterworth, a Gaussian and a flat profile

    """
    if structure_type == 'axons':
        if profile_type == 'butter':
            intensity = 1 / (1 + (dist_matrix / 2)**4)
        elif profile_type == 'flat':
            intensity = np.abs(dist_matrix) < 2
            intensity = intensity.astype(np.float)
        elif profile_type == 'gauss':
            intensity = np.exp(-dist_matrix**2 / (2*(sigma_spread**2))) / np.sqrt(2*np.pi*(sigma_spread**2)) * axons_variations

    elif structure_type == 'circle':
        if profile_type == 'butter':
            intensity = 1 / (1 + (dist_matrix / 2)**4)
        elif profile_type == 'flat':
            intensity = np.abs(dist_matrix) < 2
            intensity = intensity.astype(np.float)
        elif profile_type == 'gauss':
            intensity = np.exp(-dist_matrix**2 / (2*sigma_spread)) * brightness

    return intensity


# linear mapping function!!!
def linear_mapping(img):
    """
    This function is used to do the linear mapping

    """
    max_value = img.max()
    min_value = img.min()
    
    # build up the linear function...
    param_a = 1 / (max_value - min_value)
    param_b = 1 - max_value * param_a 
    
    # do the linear mapping...
    post_img = param_a * img + param_b
    
    return post_img

# matlab style gaussian 2D
def matlab_style_gauss2D(shape, sigma):
    """
    2D gaussian mask - should give the same results as MATLAB...
    fspecial('gaussian', [shape], [sigma])

    cite from https://stackoverflow.com/questions/17190649/how-to-obtain-a-gaussian-filter-in-python

    many thanks to the author...

    """
    m, n = [(ss - 1.)/2. for ss in shape]
    y, x = np.ogrid[-m:m+1, -n:n+1]
    h = np.exp(-(x*x + y*y) / (2.*sigma*sigma))
    h[h < np.finfo(h.dtype).eps*h.max()] = 0
    sumh = h.sum()
    if sumh != 0:
        h /= sumh
    
    return h
