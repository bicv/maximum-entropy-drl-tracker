import numpy as np
import cv2
import json

def get_completeness(gt, traj, threshold=3):
    display_traj, temp_err = [], []
    score_recoder = np.zeros((gt.shape[0],))
    num_traj = traj.shape[0]
    # do the interploation between two points
    for idx in range(num_traj - 1):
        # try to calculate the distance between 2 traj
        dist_between_two_points = np.sqrt(np.sum((traj[idx, :] - traj[idx+1, :])**2))
        if dist_between_two_points <= 6:
            # do the interpolation between two points
            interp_traj = interpolate(traj[idx, :], traj[idx+1, :])
            for inter_traj_idx in range(interp_traj.shape[0]):
                dist = np.sqrt(np.sum((interp_traj[inter_traj_idx, :] - gt) ** 2, axis=1))
                idx_sort = np.argsort(dist)
                temp_err.append(dist[idx_sort[0]])
                if dist[idx_sort[0]] <= threshold:
                    score_recoder[idx_sort[0]] += 1
                    display_traj.append(traj[idx, :])
        else:
            continue
    temp_err = np.array(temp_err)
    rmse = np.sqrt(np.mean(temp_err ** 2))
    num_non_zero = np.nonzero(score_recoder)[0].shape[0]
    completeness = num_non_zero / gt.shape[0]
    display_traj = np.array(display_traj)
    return completeness, display_traj, rmse


# load the swc data
def load_swc_data(path):
    gt_swc = open(path)
    gt = []
    for _ in range(2):
        _ = gt_swc.readline()
    # extract data
    while True:
        content = gt_swc.readline()
        if len(content) == 0:
            break
        split_info = content.split()
        gt.append([float(split_info[2]), float(split_info[3])])
    gt = np.array(gt)
    return gt

# interpolate
def interpolate(point_a, point_b):
    interp_points = []
    norm_vec = (point_b - point_a) / 100
    interp_points.append(point_a)
    tmp = point_a.copy()
    for _ in range(100):
        tmp = tmp + norm_vec
        interp_points.append(tmp)
    interp_points.append(point_b)
    return np.array(interp_points)

# read the start points from the files
def extract_start_points(path, index):
    content = open(path)
    point_recorder = []
    for _ in range(index):
        content_info = content.readline()
    split_info = content_info.split()
    # get the length of information
    iter_num = int((len(split_info) - 1) / 2)
    for idx in range(iter_num):
        x_temp = split_info[idx * 2 + 2]
        y_temp = split_info[idx * 2 + 1]
        point_recorder.append([int(x_temp), int(y_temp)])
    # return the point recorder
    return np.array(point_recorder)

# plot points on the figure
def plot_points(img, traj, color):
    for idx in range(traj.shape[0]):
        cv2.circle(img, (int(traj[idx, 0]), int(traj[idx, 1])), 1, color, -1)
    return img

if __name__ == "__main__":
    total = 0
    cov, rmse = [], []
    for img_id in range(20):
        #start = extract_start_points('start_points.txt', 18)
        # part to load the swc data
        swc_file = 'saved_swc/' + str(img_id+1).zfill(2) + '.swc'
        gt = load_swc_data(swc_file) 
        gt = gt * (250 / 512)
        gt += 50
        # get the record axon tracking path
        traj_path = 'pretrain_path/original_bs64/' + str(img_id+1).zfill(2) + '.npy'
        traj = np.load(traj_path)
        # print(gt, traj)
        # calculate the comleteness of the axon
        completeness, display_traj, temp_rmse = get_completeness(gt, traj)
        total += completeness
        cov.append(completeness)
        rmse.append(temp_rmse)
        print('the image id is: {}, the completeness is: {}, rmse is: {}'.format(img_id+1,
                                                                                 completeness, temp_rmse))
    mean_coverage = np.mean(cov)
    std_coverage = np.std(cov)
    mean_rmse = np.mean(rmse)
    std_rmse = np.std(rmse)
    print('the mean completeness is: {}'.format(mean_coverage))
    print('the std completeness is: {}'.format(std_coverage))
    print('the mean rmse is: {}'.format(mean_rmse))
    print('the std rmse is: {}'.format(std_rmse))

    # filename = 'results_test_real.json'
    # vars_dict = {'original_rmse': rmse, 'original_cov': cov}
    # json_file = json.dumps(vars_dict)
    # f = open(filename, "w")
    # f.write(json_file)
    # f.close()

    # filename = 'results_test_real.json'
    # with open(filename, 'r') as f:
    #     vars_dict = json.loads(f.read())
    # vars_dict.update({'attn_tuning_rmse': rmse,
    #                   'attn_tuning_cov': cov})
    # json_file = json.dumps(vars_dict)
    # f = open(filename, "w")
    # f.write(json_file)
    # f.close()
