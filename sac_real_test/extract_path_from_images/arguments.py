import sys; sys.argv=['']; del sys  # clear sys.argv for argparse to work in jupyter notebooks, comment for HPC
import argparse

# define the function to get the arguments...
def achieve_arguments():
    # TODO: Maybe find a better way to add the cuda...
    parse = argparse.ArgumentParser()
    
    parse.add_argument('--seed_num', type=int, default=1, help='seed number in random number generators')
    parse.add_argument('--episode_length', type=int, default=200, help='the maximum length per episode...')
    parse.add_argument('--cuda', type=bool, default=True, help='if use the GPU to train the network')
    parse.add_argument('--render', type=bool, default=True, help='render the environment')
    parse.add_argument('--st_size', type=int, default=15, help='the size of the small tracker(st)')
    parse.add_argument('--lt_size', type=int, default=31, help='the size of the larger tracker(st)')
    parse.add_argument('--precision', type=int, default=1, help='the precision of the subpixel measurement')
    parse.add_argument('--sample_point', type=int, default=100, help='the number of points that samples in the path of agent')
    parse.add_argument('--border_size', type=int, default=50, help='zero padding around the images')
    parse.add_argument('--reverse_color', type=bool, default=False, help='if reverse the color')
    parse.add_argument('--state_size', type=int, default=11, help='the size of the state size')
    parse.add_argument('--add_bouton', type=bool, default=True, help='add the boutons to the axon image')
    parse.add_argument('--add_circle', type=bool, default=True, help='add the circle to the axon image')

    # general information of the generated images...
    parse.add_argument('--width', type=int, default=128, help='the width of the generated axon images.')
    parse.add_argument('--height', type=int, default=128, help='the height of the generated axon images.')
    parse.add_argument('--max_intensity', type=int, default=255, help='the max intensity of the axon images.')
    
    # the noise parameters 
    parse.add_argument('--sigma_noise_min', type=int, default=2, help='the min gaussian level.')
    parse.add_argument('--sigma_noise_max', type=int, default=2, help='the max gaussian level.') 
    parse.add_argument('--lambda_min', type=int, default=2, help='the min possion noise level.') 
    parse.add_argument('--lambda_max', type=int, default=2, help='the max possion noise level.') 
    
    # numbers of the axons and branches...
    parse.add_argument('--min_axons', type=int, default=1, help='the minimum number of axons in the images')
    parse.add_argument('--max_axons', type=int, default=1, help='the max number of axons in the images')
    parse.add_argument('--min_bran', type=int, default=1, help='the min number of axons in the images')
    parse.add_argument('--max_bran', type=int, default=1, help='the max number of axons in the images')

    # set the shape of the axons...
    parse.add_argument('--conformity', type=float, default=0.99, help='set how straight of the axon is')
    parse.add_argument('--min_thickness', type=float, default=1.5, help='set the thickness of the axon')
    parse.add_argument('--max_thickness', type=float, default=3, help='set thickness of the axon')
    parse.add_argument('--step_size', type=int, default=2, help='the step size for constructing the axons')

    # Gaps?...I'm not sure, but I will put the parameters here...
    parse.add_argument('--min_gap_size', type=int, default=0, help='the min size of the gaps')
    parse.add_argument('--max_gap_size', type=int, default=4, help='the max size of the gaps') 

    # construction of the axons...
    parse.add_argument('--NSpline_points', type=int, default=200, help='the number of points for the interpolation of the spline')
    parse.add_argument('--crossing', type=bool, default=True, help='if allow the axon cross')
    parse.add_argument('--straight_branching', type=int, default=10, help='the distance from which overlapping \
            is checked between mother branch and daughter')

    # intensity of the axons...
    parse.add_argument('--sigma_spread', type=float, default=1, help='the std of the gaussian profile for axons')
    parse.add_argument('--min_axons_intensity', type=int, default=15, help='the min intensity value of the axons')
    parse.add_argument('--max_axons_intensity', type=int, default=40, help='the max intensity value of the axons')
    parse.add_argument('--min_period', type=float, default=0.1, help='min period for intensity alone the axons...for cosine function')
    parse.add_argument('--max_period', type=float, default=2, help='max period for intensity alone the axons...for cosine function')
    parse.add_argument('--axon_profile', default='my_cosine', help='the profile of the axon')
    parse.add_argument('--branch_profile', default='my_cosine', help='the profile of the branch')
    parse.add_argument('--sigma_noise_axons', type=float, default=0.055, help='set the white noise level for an axon')

    # shape of the synaptic boutons
    parse.add_argument('--min_nbouton', type=int, default=3, help='the minimum number of bouton')
    parse.add_argument('--max_nbouton', type=int, default=5, help='the maximum number of bouton')
    parse.add_argument('--min_brightness_bouton', type=int, default=30, help='the minimum brightness of the boutons')
    parse.add_argument('--max_brightness_bouton', type=int, default=40, help='the maximum brightness of the boutons')
    parse.add_argument('--sigma_noise_bouton', type=float, default=1, help='the noise level of the bouton')
    parse.add_argument('--min_dis_bet_bouton', type=int, default=8, help='the minimum distance between boutons')
    
    # shape of the circles
    parse.add_argument('--min_ncircle', type=int, default=13, help='set the min number of circles in the images')
    parse.add_argument('--max_ncircle', type=int, default=13, help='set the max number of circles in the images')
    parse.add_argument('--min_radius', type=int, default=3, help='the minimum radius of the circle')
    parse.add_argument('--max_radius', type=int, default=10, help='the maximum radius of the circle')
    parse.add_argument('--min_brightness_circle', type=int, default=10, help='the min intensity of the circle')
    parse.add_argument('--max_brightness_circle', type=int, default=25, help='the max intensity of the circle')
    parse.add_argument('--sigma_noise_circle', type=float, default=1.3, help='the white noise level within the circle')
    
    args = parse.parse_args()

    return args 