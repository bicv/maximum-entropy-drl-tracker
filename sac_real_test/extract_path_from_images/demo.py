import copy
import torch
import numpy as np
import original_drl_network
from arguments import achieve_arguments
from environment.real_env import real_env


# start to extract the information of start end point...
def extract_information(file_path, index):
    file_content = open(file_path)
    point_recorder = []
    for _ in range(index):
        content = file_content.readline()
    split_info = content.split()
    # get the length of the info...
    iteration_num = int((len(split_info) - 1) / 2)
    for idx in range(iteration_num):
        x_temp = split_info[idx * 2 + 2]
        y_temp = split_info[idx * 2 + 1]
        point_recorder.append([int(y_temp), int(x_temp)])
    return point_recorder


# split the start and end points...
def split_start_end_points(target, index):
    target_copy = copy.deepcopy(target)
    start_point = target_copy[index]
    target_copy.pop(index)
    return start_point, target_copy


if __name__ == '__main__':
    for img_idx in range(20):
        print('start to process the image id: {}'.format(img_idx + 1))
        # define the file path...
        img_path = 'ImagesResize/' + str(img_idx+1).zfill(2) + '.jpg'
        start_end_point_path = 'start_points.txt'
        # torch.set_default_tensor_type('torch.DoubleTensor')
        hidden_size = 256
        policy_network = original_drl_network.CNNActor(hidden_size)
        policy_network.load_state_dict(torch.load('saved_models/original_nc_alr0003.pt',
                                                  map_location=lambda storage, loc: storage))
        policy_network.eval()
        args = achieve_arguments()
        env = real_env(args)
        # start to test
        # select which image should be test...
        point_recorder = extract_information(start_end_point_path, img_idx+1)
        # set up a list to store the record of the agents...
        total_path_storage = []
        agent_measurement = []
        global_step = 0
        successful_case = 0
        for idx in range(len(point_recorder)):
            # build up the storge for each agent...
            path_temp = []
            measurement_record = []
            entropy_record = []
            # start to select the start point and end point...
            start_points, end_points = split_start_end_points(point_recorder, idx)
            with torch.no_grad():
                state = env.reset(img_path, start_points, end_points)
                path_temp.append([env.agent.real_pos[0], env.agent.real_pos[1]])
                measurement_record.append([env.agent.real_pos[1], env.agent.real_pos[0]])
                for step in range(350):
                    # Use purely exploitative policy at test time
                    action = policy_network(state).mean
                    next_state, done = env.step(action.to("cpu").detach().numpy().squeeze())
                    global_step += 1
                    path_temp.append([env.agent.real_pos[0], env.agent.real_pos[1]])
                    measurement_record.append([env.agent.real_pos[1], env.agent.real_pos[0]])
                    if done:
                        break
                    state = next_state
                total_path_storage.append(path_temp)
                for element in measurement_record:
                    agent_measurement.append(element)
        ## save as a numpy file...
        agent_measurement = np.array(agent_measurement)
        save_numpy_path = 'pretrain_path/original_nc_alr0003/' + str(img_idx + 1).zfill(2) + '.npy'
        np.save(save_numpy_path, agent_measurement)