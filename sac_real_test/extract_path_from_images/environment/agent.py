import numpy as np
import copy

"""
this module defines a agent which will take action

according to the input

2018-05-03

"""


class agent_for_tracking:
    def __init__(self, pos, precision=1, render=False):
        """ pos is the location of the agent... -> [y, x]
            precision is for the sub-pixel things """
        self.precision = precision
        self.render = render
        self.pos = np.array(pos, dtype=np.int32)
        # needs to get the non-subpixel position
        self.real_pos = self.pos / (10 ** self.precision)

    # this will move agent to the correct location according to he action
    def take_actions(self, actions):
        # copy the previous location
        pre_pos = copy.deepcopy(self.pos)
        # start to take actions...
        self.pos = self.pos + actions
        # get the real position...
        self.real_pos = self.pos / (10 ** self.precision)

        if self.render:
            if pre_pos[0] == self.pos[0] and pre_pos[1] == self.pos[1]:
                print('The tracker has been stopped...')

    # reset the location...
    def reset(self, pos):
        self.pos = np.array(pos, dtype=np.int32)
        # needs to get the non-subpixel position
        self.real_pos = self.pos / (10 ** self.precision)
