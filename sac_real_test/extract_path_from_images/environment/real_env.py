import numpy as np
# from generative_model.generator import axon_generator
import cv2
import scipy
import scipy.interpolate
import copy
import torch
from environment.agent import agent_for_tracking
from environment.utils import add_border
from environment.utils import generate_gaussian_kde


# this will create the environment for the RL based axon tracking...
class real_env:
    def __init__(self, args):
        self.args = args
        # define the agent
        self.agent = agent_for_tracking([0, 0], self.args.precision, self.args.render)

    def reset(self, image_path, start_points, end_points):
        # init the parameters...
        self.stop = False
        self.success = False
        self.track_success = False

        # load image and convert it into the grayscale
        self.original_image = cv2.imread(image_path, 0)

        # image dimensions
        self.args.width = self.original_image.shape[1]
        self.args.height = self.original_image.shape[0]

        # start to set the start point and end point...
        start_point_x = start_points[1] + self.args.border_size
        start_point_y = start_points[0] + self.args.border_size

        end_point_x = np.array([element[1] for element in end_points])
        end_point_y = np.array([element[0] for element in end_points])

        end_point_x += self.args.border_size
        end_point_y += self.args.border_size

        # process the original image
        self.original_image = self.original_image.astype(np.float32)
        self.original_image /= 255

        # add the border to the real axon image
        self.entire_image = add_border(self.original_image, self.args.border_size)

        # copy one for the visualize...
        self.visual_image = copy.deepcopy(self.entire_image)

        # check if reverse the color...
        if self.args.reverse_color:
            self.entire_image = 1 - self.entire_image

        # interpolate the entire image for the subpixel accuracy
        self.interp_image = self._interpolate_imgs(self.entire_image)

        # get the previous path image...
        self.historical_path = np.zeros(self.interp_image.shape)
        if self.args.reverse_color:
            self.historical_path = 1 - self.historical_path

        self.axons_start_point_x = start_point_x * (10 ** self.args.precision)
        self.axons_start_point_y = start_point_y * (10 ** self.args.precision)

        # set the end point....
        self.axons_end_point_x = end_point_x * (10 ** self.args.precision)
        self.axons_end_point_y = end_point_y * (10 ** self.args.precision)

        # start to reset the position of the agent...
        pos = np.array([self.axons_start_point_y, self.axons_start_point_x])
        self.agent.reset(pos)

        print("agent location: {}".format(self.agent.real_pos))

        # start to extract the states from the images...
        small_state, large_state, history_state = self._extract_states()

        # 4 observations
        small_state = np.stack([small_state, small_state, small_state, small_state], axis=0)
        large_state = np.stack([large_state, large_state, large_state, large_state], axis=0)
        history_state = np.stack([history_state, history_state, history_state, history_state], axis=0)

        # concatenate the observations together
        self.state = np.concatenate((small_state, large_state, history_state), axis=0)
        self.state_tensor = torch.tensor(self.state, dtype=torch.float32).unsqueeze(dim=0)

        return self.state_tensor

    # take the action
    def step(self, action):
        # get the previous location
        prev_loc = copy.deepcopy(self.agent.pos)

        # take the actions...
        action_interp = np.round(action, self.args.precision) * (10 ** self.args.precision)
        action_interp = action_interp.astype(np.int)
        self.agent.take_actions(action_interp)

        # get the reward from the env...
        path_recorder = self._get_reward(prev_loc)

        # will add the path to the historical path
        if path_recorder is not None:
            self._add_historical_path(path_recorder)

        # check the terminal
        terminal = self._check_terminal()

        #       self.length_counter += 1
        # if length is exceed, set terminal as True
        #         if self.length_counter >= self.args.episode_length:
        #             terminal = True

        # extract the state...
        next_small_state, next_large_state, next_history_state = self._extract_states()

        small_state = self.state[0:3, :, :]
        next_small_state = next_small_state[np.newaxis, :, :]
        small_state = np.concatenate((next_small_state, small_state), axis=0)

        large_state = self.state[4:7, :, :]
        next_large_state = next_large_state[np.newaxis, :, :]
        large_state = np.concatenate((next_large_state, large_state), axis=0)

        history_state = self.state[8:11, :, :]
        next_history_state = next_history_state[np.newaxis, :, :]
        history_state = np.concatenate((next_history_state, history_state), axis=0)

        # stack them together
        self.state = np.concatenate((small_state, large_state, history_state), axis=0)
        self.state_tensor = torch.tensor(self.state, dtype=torch.float32).unsqueeze(dim=0)

        return self.state_tensor, terminal

    # get the reward from the environment...
    def _get_reward(self, prev_loc):
        # record the path...
        path_recorder = []
        # cal the direction vector of the current location
        dir_vec, stop = self._cal_dir_vec(prev_loc)

        if stop:
            path_recorder.append(prev_loc)
            path_recorder = np.array(path_recorder)

        else:
            step_size = dir_vec / self.args.sample_point
            current_point = copy.deepcopy(prev_loc)
            path_recorder.append(current_point)
            for _ in range(self.args.sample_point):
                current_point = current_point + step_size
                # append to recorder
                path_recorder.append(current_point)
            # make them into np array...
            path_recorder = np.array(path_recorder)
            path_recorder = np.floor(path_recorder)
            path_recorder = path_recorder.astype(np.int)
            # remove the repeated term
            path_recorder = np.unique(path_recorder, axis=0)

        return path_recorder

        # check the terminals

    def _check_terminal(self):
        lt_size_interp = self.args.lt_size * (10 ** self.args.precision) - 1
        diff_large = int((lt_size_interp - 1) / 2)

        # calculate the distance to the end point
        # calculate the distance to the end
        dist = np.sqrt((self.agent.pos[0] - self.axons_end_point_y) ** 2 +
                       (self.agent.pos[1] - self.axons_end_point_x) ** 2)

        check_temp = np.where(dist <= 5 * (10 ** self.args.precision))
        if check_temp[0].shape[0] == 0:
            check_signal = False
        else:
            check_signal = True

        margin = self.args.border_size
        if (self.agent.pos[1] >= (margin - 1) * (10 ** self.args.precision) and \
                self.agent.pos[1] <= (margin + 1 + self.args.width) * (10 ** self.args.precision) and \
                self.agent.pos[0] >= (margin - 1) * (10 ** self.args.precision) and \
                self.agent.pos[0] <= (margin + 1 + self.args.height) * (10 ** self.args.precision) and \
                (not check_signal)):
            terminal = False
        else:
            terminal = True
            if (self.agent.pos[1] >= (margin - 1) * (10 ** self.args.precision) and \
                    self.agent.pos[1] <= (margin + 1 + self.args.width) * (10 ** self.args.precision) and \
                    self.agent.pos[0] >= (margin - 1) * (10 ** self.args.precision) and \
                    self.agent.pos[0] <= (margin + 1 + self.args.height) * (10 ** self.args.precision) and \
                    check_signal):
                self.track_success = True

        return terminal

    # add the historical path...
    def _add_historical_path(self, path):
        for idx in range(path.shape[0]):
            if self.args.reverse_color:
                self.historical_path[path[idx, 0] - int((10 ** self.args.precision) / 2):path[idx, 0] + int(
                    (10 ** self.args.precision) / 2) + 1, \
                path[idx, 1] - int((10 ** self.args.precision) / 2):path[idx, 1] + int(
                    (10 ** self.args.precision) / 2) + 1] = 0
            else:
                self.historical_path[path[idx, 0] - int((10 ** self.args.precision) / 2):path[idx, 0] + int(
                    (10 ** self.args.precision) / 2) + 1, \
                path[idx, 1] - int((10 ** self.args.precision) / 2):path[idx, 1] + int(
                    (10 ** self.args.precision) / 2) + 1] = 1

    # calculate the direction vector
    def _cal_dir_vec(self, prev_loc):
        dir_vec = self.agent.pos - prev_loc
        if dir_vec[0] == 0 and dir_vec[1] == 0:
            stop = True
        else:
            stop = False

        return dir_vec, stop

        # interpolate entire images...

    def _interpolate_imgs(self, entire_img):
        y_axis = np.arange(0, self.args.height + 2 * self.args.border_size)
        x_axis = np.arange(0, self.args.width + 2 * self.args.border_size)
        interpolate_img = scipy.interpolate.interp2d(x_axis, y_axis, entire_img)

        x_axis_interp = np.arange(0, self.args.width + 2 * self.args.border_size, 1 / (10 ** self.args.precision))
        y_axis_interp = np.arange(0, self.args.height + 2 * self.args.border_size, 1 / (10 ** self.args.precision))

        interp_img = interpolate_img(x_axis_interp, y_axis_interp)

        return interp_img

    # extract the states from the environment...
    def _extract_states(self):
        interp_st_size = self.args.st_size * (10 ** self.args.precision) - 1
        interp_lt_size = self.args.lt_size * (10 ** self.args.precision) - 1
        # get the state of the small size tracker...
        small_state = self.interp_image[self.agent.pos[0] - int((interp_st_size - 1) / 2):self.agent.pos[0] + int(
            (interp_st_size - 1) / 2) + 1, \
                      self.agent.pos[1] - int((interp_st_size - 1) / 2):self.agent.pos[1] + int(
                          (interp_st_size - 1) / 2) + 1]
        small_state = cv2.resize(small_state, (self.args.state_size, self.args.state_size))
        # get the state of the large size tracker...
        large_state = self.interp_image[self.agent.pos[0] - int((interp_lt_size - 1) / 2):self.agent.pos[0] + int(
            (interp_lt_size - 1) / 2) + 1, \
                      self.agent.pos[1] - int((interp_lt_size - 1) / 2):self.agent.pos[1] + int(
                          (interp_lt_size - 1) / 2) + 1]
        large_state = cv2.resize(large_state, (self.args.state_size, self.args.state_size))
        # get the state of the historical path
        history_state = self.historical_path[self.agent.pos[0] - int((interp_st_size - 1) / 2):self.agent.pos[0] + int(
            (interp_st_size - 1) / 2) + 1, \
                        self.agent.pos[1] - int((interp_st_size - 1) / 2):self.agent.pos[1] + int(
                            (interp_st_size - 1) / 2) + 1]
        history_state = cv2.resize(history_state, (self.args.state_size, self.args.state_size))

        return small_state, large_state, history_state

