import numpy as np

"""
the utils will contain the function which will be useful

for the environment

2018-05-03

"""


# this function will add the border to the image...
def add_border(image, border_size):
    # create the zero matrix...
    border_mask = np.zeros((image.shape[0] + border_size * 2, image.shape[1] + border_size * 2))
    border_mask[border_size:border_size + image.shape[0], border_size:border_size + image.shape[1]] = image

    return border_mask


def generate_gaussian_kde(data, sigma=5):
    """
    the format of the data should be -> Nx2 [x, y]

    """
    x_points = data[:, 0]
    y_points = data[:, 1]

    # build up the grid map
    X, Y = np.meshgrid(np.arange(188), np.arange(188))

    # start to process each points
    energy_map_memory = []
    num_points = data.shape[0]
    for idx in range(num_points):
        this_x = data[idx, 0]
        this_y = data[idx, 1]
        # calculate the distance of the total map...
        dist_square = (this_x - X) ** 2 + (this_y - Y) ** 2
        energy = (1 / (np.sqrt(2 * np.pi) * sigma)) * np.exp(-dist_square / (2 * np.square(sigma)))

    return linear_mapping(energy)


# linear mapping function...
def linear_mapping(img):
    """
    This function is used to do the linear mapping

    """
    max_value = img.max()
    min_value = img.min()

    # build up the linear function...
    param_a = 1 / (max_value - min_value)
    param_b = 1 - max_value * param_a

    # do the linear mapping...
    post_img = param_a * img + param_b

    return post_img