import copy
import torch
from torch import nn
from torch.nn import functional as F
from torch.distributions import Distribution, Normal

''' Soft-Actor network to output policy from pixels '''


class CNNActor(nn.Module):
    def __init__(self, hidden_size):
        super(CNNActor, self).__init__()
        # CNN layers to extract features from observations of size 12*11*11
        self.actor_CNN_layer = CNN(critic=False)
        # soft-actor network
        self.actor_net = SoftActor(hidden_size)

    def forward(self, state_actor):
        # extract features of size 32*11*11
        actor_features = self.actor_CNN_layer(state_actor)
        # obtain policy from actor network
        policy = self.actor_net(actor_features)
        return policy


''' Critic network to output the state-value function V (default) or the state-action function Q from pixels '''


class CNNCritic(nn.Module):
    def __init__(self, hidden_size, state_action):
        super(CNNCritic, self).__init__()
        # CNN layers to extract features from observations of size 16*11*11
        self.critic_CNN_layer = CNN(critic=True)
        # obtain Q or V from critic network
        self.critic_net = Critic(hidden_size, state_action)

    def forward(self, state_critic, action):
        # extract features of size 32*11*11
        critic_features = self.critic_CNN_layer(state_critic)
        # obtain Q or V from the critic network - set action to none to obtain V
        value_function = self.critic_net(critic_features, action)
        return value_function


''' Extract features using convolutional layers '''


class CNN(nn.Module):
    def __init__(self, critic=False):
        super(CNN, self).__init__()
        self.critic = critic
        self.conv1 = nn.Conv2d(16 if self.critic else 12, 32, 5, stride=1, padding=2)
        self.conv2 = nn.Conv2d(32, 32, 3, stride=1, padding=1)

    def forward(self, state):
        # shape of input: 1 x 12 (actor) or 16 (critic) x 11 x 11
        x = F.relu(self.conv1(state))  # new shape: 1 x 32 x 11 x 11
        x = F.relu(self.conv2(x))  # new shape: 1 x 32 x 11 x 11
        features = x.view(x.size(0), -1)  # reshape to (1, 32*11*11) before forwarding through fully-connected layer
        return features


''' Actor network to output the policy given features extracted using a CNN  '''


class SoftActor(nn.Module):
    def __init__(self, hidden_size):
        super(SoftActor, self).__init__()
        # Constrain range of standard deviations to prevent very deterministic/stochastic policies
        self.log_std_min, self.log_std_max = -20, 2
        self.fc1 = nn.Linear(32 * 11 * 11, hidden_size)
        self.fc2 = nn.Linear(hidden_size, hidden_size)
        self.fc3_mean = nn.Linear(hidden_size, 2)
        self.fc3_log_std = nn.Linear(hidden_size, 2)

    def forward(self, state):
        x = torch.tanh(self.fc1(state))
        x = torch.tanh(self.fc2(x))
        policy_mean = self.fc3_mean(x)
        policy_log_std = self.fc3_log_std(x)
        policy_log_std = torch.clamp(policy_log_std, min=self.log_std_min, max=self.log_std_max)
        policy = TanhNormal(policy_mean, policy_log_std.exp())
        return policy


''' Critic network to output the state-value function V (default) or the state-action function Q '''


# weight initialisation ??
class Critic(nn.Module):
    def __init__(self, hidden_size, state_action=False, layer_norm=False):
        super(Critic, self).__init__()
        self.state_action = state_action
        layers = [nn.Linear(32 * 11 * 11 + (2 if state_action else 0), hidden_size), nn.Tanh(),
                  nn.Linear(hidden_size, hidden_size), nn.Tanh(),
                  nn.Linear(hidden_size, 1)]
        # If True, insert layer normalisation between fully-connected layers and non-linearities
        if layer_norm:
            layers = layers[:1] + [nn.LayerNorm(hidden_size)] + layers[1:3] + [nn.LayerNorm(hidden_size)] + layers[3:]
        self.value = nn.Sequential(*layers)

    def forward(self, state, action=None):
        if self.state_action:
            value = self.value(torch.cat([state, action], dim=1))
        else:
            value = self.value(state)
        return value.squeeze(dim=1)


# ------------------------------------------------ Utility Functions ------------------------------------------------- #
''' Target value function V_phi with non-differentiable network parameters '''


def TargetCritic(network):
    target_network = copy.deepcopy(network)
    for param in target_network.parameters():
        param.requires_grad = False
    return target_network


''' Update the target network '''


def update_target_network(network, target_network, polyak_factor):
    for param, target_param in zip(network.parameters(), target_network.parameters()):
        target_param.data = polyak_factor * target_param.data + (1 - polyak_factor) * param.data


class TanhNormal(Distribution):
    def __init__(self, loc, scale):
        super(TanhNormal, self).__init__()
        self.normal = Normal(loc, scale)

    def sample(self):
        return torch.tanh(self.normal.sample())

    # use the re-parameterisation trick from the rsample() method, where the parameterised random variable can be
    # constructed via a parameterised deterministic function of a parameter-free random variable.
    # The reparameterised sample therefore becomes differentiable.
    def rsample(self):
        return torch.tanh(self.normal.rsample())

    # Calculates log probability of value using the change-of-variables technique
    # (uses log1p = log(1 + x) for extra numerical stability)
    def log_prob(self, value):
        inv_value = (torch.log1p(value) - torch.log1p(-value)) / 2  # arctanh(y)
        return self.normal.log_prob(inv_value) - torch.log1p(
            -value.pow(2) + 1e-6)  # log p(f^-1(y)) + log |det(J(f^-1(y)))|

    def rsample_and_log_prob(self):
        value = self.normal.rsample()
        log_prob = self.normal.log_prob(value)
        value = torch.tanh(value)
        log_prob -= torch.log1p(-value.pow(2) + 1e-6)
        return value, log_prob

    @property
    def mean(self):
        return torch.tanh(self.normal.mean)
