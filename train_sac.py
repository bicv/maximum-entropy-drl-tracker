""" Script to train the soft-actor critic network with two soft-Q functions for the maximum-
    entropy DRL neural tracker.  It includes an option for automatic entropy tuning and an
    initial exploration phase.
    Author: Shafa Balaram
    Date: 14th June 2019
    Based on Kai's and Vitchyr's soft actor-critics available at:
    https://github.com/Kaixhin/spinning-up-basic and
    https://github.com/vitchyr/rlkit/tree/master/rlkit/torch/sac respectively."""


''' Modules '''
import os
import copy
import json
import torch
import random
import numpy as np
from torch import nn
from tqdm import tqdm
from torch import optim
from collections import deque
from environment.env import axon_env
from arguments import achieve_arguments
from drl_network import CNNActor, CNNCritic, TargetCritic, update_target_network 


''' Hyperparameters '''
MAX_STEPS = 200000 # This was set to 250k
REPLAY_SIZE = 250000 # Set to 250k
UPDATE_START = 50000 # exploration phase - was 50000 in original....
UPDATE_INTERVAL = 1 # for gradient updates
TEST_INTERVAL = 1000 # for validation during training
BATCH_SIZE = 64

HIDDEN_SIZE = 256 # size of each hidden layer in actor-critic network
ACTOR_LEARNING_RATE = 0.0003
CRITIC_LEARNING_RATE = 0.0001 
EPS = 1e-8 # epsilon to improve stability of Adam optimiser
WEIGHT_DECAY = 0.0002 # L2-norm penalty

AUTOMATIC_ENTROPY_TUNING = False # default
TARGET_ENTROPY = None # can be specified for automatic weight tuning, else it is set to -dim(A)
REWARD_SCALE = 1
POLYAK_FACTOR = 0.995 # target smoothing coefficient τ
DISCOUNT = 0.99 # discount factor γ


''' Validation function on the 20 synthetic images generated after the initial seed number provided. Actions are obtained as the mean of the policy's distribution to evaluate exploitation ability of agent. The function returns the total rewards and percentage of axons in the validation dataset that are successfully tracked (when the agent reaches within 5 pixels of the end-point). '''
def test(seed_num, actor, device):
    episode_reward, tracking_success = [], 0  
    for index in range(1, 21): 
        env_id = seed_num + index  
        with torch.no_grad():
            # init the environment
            args = achieve_arguments()
            env = axon_env(args)     
            # reset the environment
            state_actor, state_critic = env.reset(env_id)
            # terminal state and total reward for episode
            done, total_reward = False, 0
            while not done:
                # Use purely exploitative policy at test time
                action = actor(state_actor.to(device)).mean  
                next_state_actor, next_state_critic, reward, done = env.step(action.to("cpu").detach().numpy().squeeze())
                state_actor = next_state_actor
                state_critic = next_state_critic
                total_reward += reward

        episode_reward.append(total_reward)
        if env.success:
            tracking_success += 1

    episode_reward = np.sum(episode_reward)     
    tracking_success =  tracking_success / 20 * 100 
    return episode_reward, tracking_success


''' Initialisations '''
# check if CUDA is available
use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")
print('Device available: {}'.format(device))

# learning rates of the actor-critic networks
print("Learning rate for the actor and critic networks respectively: {}, {}".format(ACTOR_LEARNING_RATE, CRITIC_LEARNING_RATE))

# initialise arguments for the synthetic axon generator
args = achieve_arguments()
# initialise the agent's environment
env = axon_env(args)

# initialise the starting image number for the axon generators used during training and validation
train_seed_num = 1
test_seed_num = 100000

# seed the random number generators for training axon generator
args.seed_num = train_seed_num
random.seed(args.seed_num)
np.random.seed(args.seed_num)
torch.manual_seed(args.seed_num)

# initialise actor-critic networks to output the policy, soft-Q1, soft-Q2 and target critics
actor = CNNActor(HIDDEN_SIZE).to(device)
critic1 = CNNCritic(HIDDEN_SIZE, state_action=True).to(device)
critic2 = CNNCritic(HIDDEN_SIZE, state_action=True).to(device)
target_critic1 = TargetCritic(critic1).to(device)
target_critic2 = TargetCritic(critic2).to(device)

# needed for automatic entropy tuning
if AUTOMATIC_ENTROPY_TUNING:
    if TARGET_ENTROPY:
        TARGET_ENTROPY = TARGET_ENTROPY
    else:
        action_space_dim = 2 
        TARGET_ENTROPY = -np.prod(action_space_dim) 
    log_alpha = torch.zeros(1, requires_grad=True, device=device)
    alpha_optimiser = optim.Adam([log_alpha], lr=ACTOR_LEARNING_RATE)
    
# Adam optimisers: the 2 critics share the same weights
# eps is a small number added to improve numerical stability and the weight decay is an L2-norm
# penalty term added as regularisation to improve generalisation
actor_optimiser = optim.Adam(actor.parameters(), lr=ACTOR_LEARNING_RATE, eps=EPS, weight_decay=WEIGHT_DECAY)
critic1_optimiser = optim.Adam(critic1.parameters(), lr=CRITIC_LEARNING_RATE, eps=EPS, weight_decay=WEIGHT_DECAY)
critic2_optimiser = optim.Adam(critic2.parameters(), lr=CRITIC_LEARNING_RATE, eps=EPS, weight_decay=WEIGHT_DECAY)

# buffer replay
D = deque(maxlen=REPLAY_SIZE)

# path to save trained model and variables
model_path = './models/'
vars_path = './results/'
if not os.path.exists(model_path):
    os.mkdir(model_path)
if not os.path.exists(vars_path):
    os.mkdir(vars_path)
    
# terminal state and variables to store validation results
done = False
steps, step_episode_reward, step_success = [], [], []

# reset the environment and agent at the starting point of the axon
state_actor, state_critic = env.reset(args.seed_num)

# progress bar
pbar = tqdm(range(1, MAX_STEPS + 1), unit_scale=1, smoothing=0)


''' Train Soft Actor-Critic Network '''
for step in pbar:
    with torch.no_grad():
        if step < UPDATE_START:
            # To improve exploration take actions sampled from a uniform random distribution over actions at the start of training
            action = torch.tensor([[2 * random.random() - 1 for _ in range(2)]], dtype=torch.float32)
        else:
            # Observe state s and select action a ~ μ(a|s)
            action = actor(state_actor.to(device)).sample()
            action = action.to("cpu")
        # Execute a in the environment and observe next state s', reward r, and done signal d to indicate whether s' is terminal    
        next_state_actor, next_state_critic, reward, done = env.step(action.detach().numpy().squeeze())

        # Store timestep (s, a, r, s', d) in replay buffer D
        D.append({'state_actor': state_actor, 'state_critic': state_critic, 
                  'action': action, 'reward': torch.tensor([reward], dtype=torch.float32), 
                  'next_state_actor': next_state_actor, 'next_state_critic': next_state_critic,
                  'done': torch.tensor([done], dtype=torch.float32)})

        # update the current states
        state_actor = copy.deepcopy(next_state_actor)
        state_critic = copy.deepcopy(next_state_critic)

        # If s' is terminal, reset environment state
        if done:
            args.seed_num += 1 # generate new synthetic image
            state_actor, state_critic = env.reset(args.seed_num) # reset agent
    if step > UPDATE_START and step % UPDATE_INTERVAL == 0:            

        # Randomly sample a batch of transitions B = {(s, a, r, s', d)} from D
        batch = random.sample(D, BATCH_SIZE)
        batch = {k: torch.cat([d[k] for d in batch], dim=0) for k in batch[0].keys()}
        ''' Policy π at time t '''
        # a(s) is a sample from μ(·|s) which is differentiable wrt θ via the reparameterisation trick
        policy = actor(batch['state_actor'].to(device))  
        # Note: in practice it is numerically more stable to calculate the log probability when sampling an action to avoid inverting tanh
        action, log_prob = policy.rsample_and_log_prob()

        ''' Stochastic gradient descent of temperature parameter, α, with an Adam optimiser'''
        if AUTOMATIC_ENTROPY_TUNING:
            alpha_loss = -(log_alpha * (log_prob + TARGET_ENTROPY).detach()).mean()
            alpha_optimiser.zero_grad()
            alpha_loss.backward()
            alpha_optimiser.step()
            alpha = log_alpha.exp()
        else:
            alpha_loss = 0
            alpha = 1  # if fixed, temperature parameter can be set to a different value here

        # Q = min{ Q1, Q2 }
        q_new_actions = torch.min(critic1(batch['state_critic'].to(device), action), 
                                  critic2(batch['state_critic'].to(device), action))
        policy_loss = (alpha*log_prob.sum(dim=1) - q_new_actions).mean()
        
        ''' Q loss '''
        q1_pred = critic1(batch['state_critic'].to(device),batch['action'].to(device))
        q2_pred = critic2(batch['state_critic'].to(device),batch['action'].to(device))
        
        # Policy π at time t+1: a(s') is a sample from μ(·|s) which is differentiable wrt θ via the reparameterisation trick
        next_policy = actor(batch['next_state_actor'].to(device))  
        
        # Note: in practice it is more numerically stable to calculate the log probability when sampling an action to avoid  
        # inverting tanh 
        next_action, next_log_prob = next_policy.rsample_and_log_prob()
        
        # Target Q
        target_q1 = target_critic1(batch['next_state_critic'].to(device), next_action)
        target_q2 = target_critic2(batch['next_state_critic'].to(device), next_action)
        target_q = torch.min(target_q1, target_q2) - alpha * next_log_prob.sum(dim=1)
        y_q = ((REWARD_SCALE * batch['reward']) + (DISCOUNT * (1 - batch['done']) * target_q.to("cpu"))).to(device)
        
        # MSE losses
        q1_loss = (q1_pred - y_q.detach()).pow(2).mean()
        q2_loss = (q2_pred - y_q.detach()).pow(2).mean()
        
        ''' Update SAC networks '''
        # Soft-Q1 network
        critic1_optimiser.zero_grad()
        q1_loss.backward()
        critic1_optimiser.step()
        
        # Soft-Q2 network
        critic2_optimiser.zero_grad()
        q2_loss.backward()
        critic2_optimiser.step()
        
        # Policy network
        actor_optimiser.zero_grad()
        policy_loss.backward()
        actor_optimiser.step()
        
        ''' Soft updates of the two target networks'''
        update_target_network(critic1, target_critic1, POLYAK_FACTOR)    
        update_target_network(critic2, target_critic2, POLYAK_FACTOR) 
        
    ''' Validation '''
    if step > UPDATE_START and step % TEST_INTERVAL == 0:
        actor.eval()
        episode_reward, tracking_success = test(test_seed_num, actor, device)
        pbar.set_description('Step: %i | Total Reward: %f | Success: %f' % (step, episode_reward, tracking_success))
        steps.append(step)
        step_episode_reward.append(episode_reward)
        step_success.append(tracking_success)
        actor.train()
        
print("number of training images: {}".format(args.seed_num))

# Bookkeeping
torch.save(actor.state_dict(), model_path + 'sac_model.pt') # final trained policy network
filename = vars_path + 'validation_vars.json' # validation results
vars_dict = {'steps': steps, 'total_rewards': (np.array(step_episode_reward, np.float32)).tolist(),
             'success': step_success}
json_file = json.dumps(vars_dict)
f = open(filename,"w")
f.write(json_file)
f.close() 
