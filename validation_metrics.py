import copy
import json
import torch
import numpy as np
import lcn_drl_network
from arguments import achieve_arguments
from environment.env import axon_env
from generative_model.generator import axon_generator


def get_completeness(gt, traj, threshold=3):
    display_traj, temp_err = [], []
    score_recoder = np.zeros((gt.shape[0],))
    num_traj = traj.shape[0]
    # do the interploation between two points
    for idx in range(num_traj - 1):
        # try to calculate the distance between 2 traj
        dist_between_two_points = np.sqrt(np.sum((traj[idx, :] - traj[idx+1, :])**2))
        if dist_between_two_points <= 6:
            # do the interpolation between two points
            interp_traj = interpolate(traj[idx, :], traj[idx+1, :])
            for inter_traj_idx in range(interp_traj.shape[0]):
                dist = np.sqrt(np.sum((interp_traj[inter_traj_idx, :] - gt) ** 2, axis=1))
                idx_sort = np.argsort(dist)
                temp_err.append(dist[idx_sort[0]])
                if dist[idx_sort[0]] <= threshold:
                    score_recoder[idx_sort[0]] += 1
                    display_traj.append(traj[idx, :])
        else:
            continue
    temp_err = np.array(temp_err)
    rmse = np.sqrt(np.mean(temp_err ** 2))
    num_non_zero = np.nonzero(score_recoder)[0].shape[0]
    completeness = num_non_zero / gt.shape[0]
    display_traj = np.array(display_traj)
    return completeness, display_traj, rmse


# interpolate
def interpolate(point_a, point_b):
    interp_points = []
    norm_vec = (point_b - point_a) / 100
    interp_points.append(point_a)
    tmp = point_a.copy()
    for _ in range(100):
        tmp = tmp + norm_vec
        interp_points.append(tmp)
    interp_points.append(point_b)
    return np.array(interp_points)


if __name__ == '__main__':
    # seed number used to later specify the image used for validation
    seed_num = 100000
    # margin used in the calculation of the coverage
    threshold = 3
    # fetch parameters for axon generator
    args = achieve_arguments()
    # initialise the generator
    generator = axon_generator(args)
    # initialise the environment
    env = axon_env(args)
    # load trained soft actor-critic network
    device = torch.device("cpu")
    HIDDEN_SIZE = 256
    policy_network = lcn_drl_network.CNNActor(HIDDEN_SIZE)
    policy_network.load_state_dict(torch.load('models/lcn_nc_bs64.pt',
                                   map_location=lambda storage, loc: storage))
    policy_network.eval()
    # set up a list to store the record of the agents...
    cov, rmse = [], []
    for img_idx in range(1, 21):
        ''' Trajectory '''
        env_id = seed_num + img_idx
        # generate the validation image
        _, _, gt = generator.generate_imgs(env_id)
        # obtain the predicted trajectory
        pred_traj = []
        with torch.no_grad():
            state_actor, state_critic = env.reset(env_id)
            pred_traj.append(env.agent.real_pos - args.border_size)
            # terminal state and total reward for episode
            done, total_reward = False, 0
            while not done:
                # Use purely exploitative policy at test time
                action = policy_network(state_actor).mean
                next_state_actor, next_state_critic, reward, done = \
                    env.step(action.to("cpu").detach().numpy().squeeze())
                state_actor = next_state_actor
                state_critic = next_state_critic
                pred_traj.append(env.agent.real_pos - args.border_size)
        pred_traj = np.array(pred_traj)
        pred_traj = np.array(pred_traj)

        ''' Results Calculation '''
        # calculate the comleteness of the axon
        completeness, _, temp_rmse = get_completeness(gt, pred_traj)
        cov.append(completeness)
        rmse.append(temp_rmse)
        print('the image id is: {}, the completeness is: {}, rmse is: {}'.format(env_id,
                                                                                 completeness, temp_rmse))
    mean_coverage = np.mean(cov)
    std_coverage = np.std(cov)
    mean_rmse = np.mean(rmse)
    std_rmse = np.std(rmse)
    print('the mean completeness is: {}'.format(mean_coverage))
    print('the std completeness is: {}'.format(std_coverage))
    print('the mean rmse is: {}'.format(mean_rmse))
    print('the std rmse is: {}'.format(std_rmse))

    # filename = 'results_validation_new.json'
    # vars_dict = {'original_bs64_rmse': rmse, 'original_bs64_cov': cov}
    # json_file = json.dumps(vars_dict)
    # f = open(filename, "w")
    # f.write(json_file)
    # f.close()

#    filename = 'results_validation_new.json'
#    with open(filename, 'r') as f:
#        vars_dict = json.loads(f.read())
#    vars_dict.update({'lcn_nc_bs64_rmse': rmse,
#                      'lcn_nc_bs64_cov': cov})
#    json_file = json.dumps(vars_dict)
#    f = open(filename, "w")
#    f.write(json_file)
#    f.close()

